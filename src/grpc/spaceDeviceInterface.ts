export interface DeviceMessage {
    transactionNo: number;
    NISPatientId: string;
    boxDeviceId: string;
    name: string;
    location: string;
    room: string;
    event?: string;
    state?: string;
    description?: string;
    message: string;
    occurredAt: string;

}

export interface DeviceMessageResp {
    transactionNo: number;
    serverResp: string;
}

