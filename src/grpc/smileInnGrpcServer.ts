"use strict";
export {}
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

const SMILE_PROTO_PATH = __dirname + '/protos/smileInnStatus.proto';
const TENANT_PROTO_PATH = __dirname + '/protos/smileInnTenant.proto';
const DEVICE_MESSAGE_PROTO_PATH = __dirname + '/protos/spaceDeviceMessage.proto';
const MINICARE_PROTO_PATH = __dirname + '/protos/miniCare.proto';


class JuboSpaceGrpcServer {
    
  smileInnStatusDefinition : any;
  smileInnTenantDefinition : any; 
  deviceMessageDefinition : any;
  miniCareDefinition : any; 

  miniCare_proto: any;
  smileInnStatus_proto: any;
  smileInnTenant_proto : any;
  deviceMessage_proto : any;

  grpcServer : any;
  serverUri : string;

  constructor(address: string|number, port: string|number) {

    this.miniCareDefinition - protoLoader.loadSync(
      MINICARE_PROTO_PATH,
      {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
      });

    this.smileInnStatusDefinition = protoLoader.loadSync(
      SMILE_PROTO_PATH,
      {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
      });
        
    this.smileInnTenantDefinition = protoLoader.loadSync(
      TENANT_PROTO_PATH,
      {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
      }); 

    this.deviceMessageDefinition = protoLoader.loadSync(
      DEVICE_MESSAGE_PROTO_PATH,
      {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
      }
    )

    this.miniCare_proto = grpc.loadPackageDefinition(this.miniCareDefinition).miniCareToSpacePackage;

    //self test
    this.smileInnStatus_proto = grpc.loadPackageDefinition(this.smileInnStatusDefinition).smileinnstatus;
    this.smileInnTenant_proto = grpc.loadPackageDefinition(this.smileInnTenantDefinition).smileinntenant;
    this.deviceMessage_proto = grpc.loadPackageDefinition(this.deviceMessageDefinition).spacedevicemessage;
    this.initGrpcServer(address, port)

  }

  private initGrpcServer =  (address: string|number, port: string|number) => {
    this.serverUri = address + ':' + port;
    this.grpcServer = new grpc.Server();

    this.grpcServer.addService(this.deviceMessage_proto.DeviceMessenger.service, {
      DeviceEventStub : this.replyDeviceEvent,
      DeviceStateStub : this.replyDeviceState
    })
    //this.grpcServer.addService(this.smileInnStatus_proto.SensorUpdater.service, {SensorReportStub: this.replySensorReport});
    this.grpcServer.addService(this.smileInnStatus_proto.StatusUpdater.service, {
      EquipmentStatusStub : this.replyEquipAlert,
      NoMovementStatusStub : this.replyPanicAlert,
      PanicButtonStatusStub : this.replyNoMovementAlert,
      FallDetectStatusStub: this.replyFallAlert,
      PresenceDetectStatusStub: this.replyPresenceStatus,
      SleepInfoReportStub: this.replySleepInfo,
      BedStatusUpdateStub: this.replyBedUpdate,
      BedSettingUpdateStub: this.replyBedSetting,

      tenantCheckInStub : this.tenantCheckInHandler,


    });

    this.grpcServer.bind(this.serverUri, grpc.ServerCredentials.createInsecure());
    this.grpcServer.start();
  }

  public replyDeviceEvent = (call, callback) => {
    console.log(call)
    callback(null, {
      transactionNo: call.request.transactionNo,
      serverResp: "resp DeviceEvent from server"
    })
  }

  public replyDeviceState = (call, callback) => {
    console.log(call)
    callback(null, {
      transactionNo: call.request.transactionNo,
      serverResp: "resp DeviceState from server"
    })
  }

  public replySensorReport = (call, callback) => {
    console.log(call)
    callback(null, {
        transactionNo: call.request.transactionNo,
        serverResp: "resp SensorReport from server"
      });
  }

  public replyFallAlert = (call, callback) => {
    console.log(call)
    callback(null, {
        transactionNo: call.request.transactionNo,
        serverResp: "resp fallAlert from server"
      });
  }

  public replyPresenceStatus = (call, callback) => {
    console.log(call)
    callback(null,{
      transactionNo: call.request.transactionNo,
      serverResp: 'resp presenceStatus from server'
    })
  }

  public replySleepInfo = (call, callback) => {
    console.log(call)
    callback(null, {
        transactionNo: call.request.transactionNo,
        serverResp: "resp sleepInfo from server"
      });
  }
  
  public replyBedUpdate = (call, callback) => {
    console.log(call)
    callback(null, {
        transactionNo: call.request.transactionNo,
        serverResp: "resp bedUpdate from server"
      });
  }
  
  public replyBedSetting = (call, callback) => {
    console.log(call)
    callback(null, {
      transactionNo: call.request.transactionNo,
      serverResp: 'resp bedSetting from server'
    })
  }

  public replyEquipAlert = (call, callback) => {
    console.log(call)
    callback(null, {
        transactionNo: call.request.transactionNo,
        serverResp: "resp EquipAlert from server"
      });
  }

  public replyPanicAlert = (call, callback) => {
    console.log(call)
    callback(null, {
        transactionNo: call.request.transactionNo,
        serverResp: "resp PanicAlert from server"
      });
  }
  
  public replyNoMovementAlert = (call, callback) => {
    console.log(call)
    callback(null, {
        transactionNo: call.request.transactionNo,
        serverResp: "resp NoMovementAlert from server"
    });
  }  

  public tenantCheckInHandler = (call, callback) => {
    console.log(call)
    callback(null, {
        transactionNo: call.request.transactionNo,
        serverResp: "resp tenantCheckInUpdate from server"
    });    
  }

  public onDashBoardRequest = (call, callback) => {
    console.log(call)
    callback(null, {
      transactionNo: call.request.transactionNo,
      serverResp: "resp onDashBoardRequest from server"
    })
  }  
}

let juboSpaceGrpcHandler = new JuboSpaceGrpcServer('0.0.0.0', '50051')
exports.grpcInwardHandler = juboSpaceGrpcHandler 
//module.exports = JuboSpaceGrpcServer
