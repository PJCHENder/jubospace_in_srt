export interface ReportUpdate {
    transactionNo: number;
    name: string;
    room: string;
    type: string;
    sensorReport: SensorReportValue;    
}

export interface SensorReportValue {
    homeId: string;
    deviceId: string;
    name: string;    
    sensortime: number;
    powerLevel: number;
    productId: string;
    functype: string;
    group: string;
    location: string;
    userDefineName: string;
    sensorValue: number;
}

export interface ReportResp {
    transactionNo: number;
    serverResp: string;
}

export interface EquipmentStatusUpdate {
    transactionNo: number;
    name: string;
    room: string;
    type: string;
    statusUpdateTime: number;
    powerLevel: number;
    productId: string;
    funcType: string;
    group: string;
    location: string;
    userDefineName: string;
    message: string;        
}

export interface PanicButtonStatusUpdate {
    transactionNo: number;
    name: string;
    room: string;
    type: string;
    statusUpdateTime: number;
    group: string;
    userDefineName: string;
    location: string; 
    message: string;
}

export interface FallDetectStatusUpdate {
    transactionNo: number;
    name: string;
    room: string;
    type: string;
    statusUpdateTime: number;
    group: string;
    userDefineName: string;
    location: string;
    message: string;     
}

export interface NoMovementStatusUpdate {
    transactionNo: number;
    name: string;
    room: string;
    type: string;
    noMovementSince: number;
    statusUpdateTime: number;
    userDefineName: string;
    group: string;
    location: string;    
}

export interface SleepInfoReport {
    "transactionNo": number;
    "room": string;
    "type":   string;                   //"MWG-Sleep-Info"
    "mattressMac": string;             //"01BA4C1A",
    "sleepStartTime": string;          //"2020-07-20 11:01:40",
    "sleepEndTime": string;            //"2020-07-20 12:03:16",
    "sleepLatency": number;           //1965 (sec)
    "sleepEffectiveness": number;       //54.167
    "turnOverCnt": string;               //"5"
    "notInBedCnt": string;          //"0"
    "totalNotInBedTime": number;         //1234
    "message": string;                 //"睡眠資訊"
    "patientName"?: string;              //"周阿德"
    "patientId" : string;                //"A213459803"

}

export interface BedStatusReport {
    "transactionNo": number;
    "room": string;
    "type":   string;                   //"MWG-Sleep-Info"
    "mattressMac": string;             //"01BA4C1A",
    "bedStateAlertLevel":string;        //"0 - 不警報，一般狀態顯示"
                                        //"1 - 不警報，但帶提醒作用"
                                        //"2 - 發警報，提醒人員處理"
    "bedState":string;                  //"15 - 側躺"
                                        //"1 - 不在床(完全離床)"
                                        //"13 - 進床"
                                        //"4 - 在床(臥床)"
                                        //"14 - 準備離床(坐在床緣)"
                                        //"15 - 側躺"  
                                        //"12 - 起身(以坐姿在床上)"
                                        //"203 - 床墊接頭脫落"
                                        //"206 - 離線"
                                        //"217 - 久臥(翻身拍背提醒)"
                                        //"218 - 離床過久"
    "message":string;                 //"床墊資訊"   
}

export interface MwgBedSetting {
    
}

export interface StatusUpdateResp {
    transactionNo: number;
    serverResp: string;
}



export interface TenantCheckInInfo {
    nativeLanguage: string[];
    _id: string;                         // "5ea6ae277cdcab06534247dd"
    sex: string;                         // "female"
    branch: string;                      // "1"
    room: string;                        // "707"
    bed: string;                         // "1"
    idNumber: string;                    // "P188052012"
    lastName: string;                    // "Chou"
    firstName: string;                   // "Derick"
    title: string;                      // "Mr. "
    status: string;                     // "present"
    organization: string;               // "5ea6ac857cdcab06534247dc" 
    birthday: string;                   // "1963-12-10T11:37:00.000Z" 
    checkInDate: string;                // "2020-05-26T11:37:00.000Z"
    checkOutDate: string;                // "2020-06-26T11:37:00.000Z"  
    religion: string[];          // "[ ]", not yet implemented
    habits: string[];            // "[ ]", not yet implemented
    medications: string[];       // "[ ]", not yet implemented
    drugAllergies: string[];     // "[ ]", not yet implemented
    foodAllergies: string[];     // "[ ]", not yet implemented

    token: string;                      // "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlYTZhZTI3N2NkY2FiMDY1MzQyNDdkZCIsInJvbGVzIjpbIm9yZ2FuaXphdGlvbi1tYW5hZ2VyIl0sImlhdCI6MTU5MDM5MDQ1NywiZXhwIjoxNTkwNDMzNjU3fQ.OxGRvgPDKy5OkzaXFtRZkevFuZ-nQ1QquzMTqzhSl0k"
    
}

export interface TenantCheckInResp {
    room: string;
    message: string;
}

