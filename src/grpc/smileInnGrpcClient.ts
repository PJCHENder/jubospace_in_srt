"use strict";

const SMILE_PROTO_PATH = __dirname + '/protos/smileInnReport.proto';
const STATUS_PROTO_PATH = __dirname + '/protos/smileInnStatus.proto';

const utils = require('./../utils/utilities')

const TRANS_MAX = 100000
const grpcClientIp = '127.0.0.1'
const grpcClientPort = '50051'

import {    
            ReportUpdate,
            EquipmentStatusUpdate,
            PanicButtonStatusUpdate,
            NoMovementStatusUpdate,
            FallDetectStatusUpdate,
            SleepInfoReport,
            BedStatusReport,
            StatusUpdateResp
        } from "./smileInnGrpcInterface";

const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const smileInnStatusDefinition = protoLoader.loadSync(
    STATUS_PROTO_PATH,
    {keepCase: true,
      longs: String,
      enums: String,
      defaults: true,
      oneofs: true
    });


const smileInnStatus_proto = grpc.loadPackageDefinition(smileInnStatusDefinition).smileinnstatus;

class SmileInnGrpcClient {
    uri: string;
    transactionNumber: number;
    spaceStatusHandler: any;
    timerHandler: any;

    constructor(address: string|number, port: string|number){
        this.uri = address + ':' + port;
        this.transactionNumber = 1;
        this.spaceStatusHandler = new smileInnStatus_proto.StatusUpdater(this.uri,
        grpc.credentials.createInsecure());
        this.timerHandler = setInterval(this.serverTimerLoop.bind(this),2000)

    }

    private serverTimerLoop = () => {
        try {
            if(this.transactionNumber >= TRANS_MAX){
                this.transactionNumber = 1;
            }
        } catch (error) {
            console.log(`fn: serverTimerLoop ${error}`)
            
        } 
    }

    public EquipmentStatusUpdate = (smileInnStatusReport: EquipmentStatusUpdate) => {
        return new Promise ((resolve,reject) => {
            try{
                this.spaceStatusHandler.EquipmentStatusStub(smileInnStatusReport, 
                        (err,response) => {
                            if(err){reject(err)}
                            console.log("in EquipmentAlert")
                            console.log(response);
                            resolve(response);                    
                })
            } catch (error){
                reject(error)
            }
        })
    }
    public PanicButtonGrpcUpdate = (smileInnStatusReport: PanicButtonStatusUpdate) => {
        return new Promise ((resolve,reject) => {
            try{
                this.spaceStatusHandler.PanicButtonStatusStub(smileInnStatusReport, 
                    (err,response) => {
                        if(err){reject(err)}
                        console.log("in PanicButtonAlert")
                        console.log(response);
                        this.transactionNumber++;
                        resolve(response);                       
                })

            } catch (error){
                reject(error)
            }
        })
    }

    public FallDetectStatusUpdate = (fallDetectReport : any) => {
        return new Promise ((resolve,reject) => {
            try{
                console.log(fallDetectReport)
                this.spaceStatusHandler.FallDetectStatusStub({
                    "transactionNo": this.transactionNumber.toString(),
                    "name": fallDetectReport.name,
                    "room": fallDetectReport.room,
                    "type": fallDetectReport.type,
                    "statusUpdateTime": utils.formatDate(fallDetectReport.statusUpdateTime,"Y-m-d H:i:s"),
                    "location": fallDetectReport.location,
                    "message": fallDetectReport.message

                },(err, resp) => {
                    if(err){reject(err)}
                    console.log("in FallDetectStatusUpdate")
                    console.log(resp);
                    this.transactionNumber++;
                    resolve(resp);                          
                })
            } catch (error) {
                reject(error)
            }
        })
    }

    public NoMovementStatusUpdate = (noMovementReport: NoMovementStatusUpdate) => {
        return new Promise ((resolve,reject) => {
            try{
                this.spaceStatusHandler.NoMovementStatusStub({
                    "transactionNo": this.transactionNumber.toString(),
                    "name": noMovementReport.name,
                    "room": noMovementReport.room,
                    "type": noMovementReport.type,
                    "noMovementSince": noMovementReport.noMovementSince,
                    "statusUpdateTime": noMovementReport.statusUpdateTime,
                    "group": noMovementReport.group,       //Pir感應 毫米波感應
                    "location": noMovementReport.location   
                }, (err,response) => {
                    if(err){reject(err)}
                    console.log("in NoMovementAlert")
                    console.log(response);
                    this.transactionNumber++;
                    resolve(response);                    
                })

            } catch (error){
                reject(error)
            }
        })
    }

    public PresenceDetectStatusUpdate = (presenceReport : any) => {
        return new Promise ((resolve,reject) => {
            try {
                this.spaceStatusHandler.PresenceDetectStatusStub({
                    "transactionNo": this.transactionNumber.toString(),
                    "name": presenceReport.name,
                    "room": presenceReport.room,
                    "type": presenceReport.type,
                    "statusUpdateTime": utils.formatDate(presenceReport.statusUpdateTime,"Y-m-d H:i:s"),
                    "location": presenceReport.location,
                    "message": presenceReport.message
                }, (err,resp) => {
                    if(err){reject(err)}
                    this.transactionNumber++
                    console.log(resp)
                    resolve(resp)                      
                })
            } catch (error) {

            }
        })
    }

    public PresenceCancelStatusUpdate = (presenceReport : any) => {
        return new Promise ((resolve,reject) => {
            try {
                console.log('PresenceCancelStatusUpdate')
                this.spaceStatusHandler.PresenceDetectStatusStub({
                    "transactionNo": this.transactionNumber.toString(),
                    "name": presenceReport.name,
                    "room": presenceReport.room,
                    "type": presenceReport.type,
                    "statusUpdateTime": utils.formatDate(presenceReport.statusUpdateTime,"Y-m-d H:i:s"),
                    "location": presenceReport.location,
                    "message": presenceReport.message

                }, (err,resp) => {
                    if(err){reject(err)}
                    this.transactionNumber++
                    console.log(resp)
                    resolve(resp)
                })
            } catch (error) {

            }
        })
    }

    public SleepInfoReport = (sleepInfoReport: SleepInfoReport) => {
        return new Promise ((resolve,reject) => {
            try{
                this.spaceStatusHandler.SleepInfoReportStub({
                    "transactionNo": this.transactionNumber.toString(),
                    "room": sleepInfoReport.room,
                    "type": sleepInfoReport.type,                   //"MWG-Sleep-Info"
                    "mattressMac": sleepInfoReport.mattressMac,             //"01BA4C1A",
                    "sleepStartTime": sleepInfoReport.sleepStartTime,          //"2020-07-20 11:01:40",
                    "sleepEndTime": sleepInfoReport.sleepEndTime,            //"2020-07-20 12:03:16",
                    "sleepLatency": sleepInfoReport.sleepLatency,           //1965 (sec)
                    "sleepEffectiveness": sleepInfoReport.sleepEffectiveness,       //54.167
                    "turnOverCnt": sleepInfoReport.turnOverCnt,               //"5"
                    "notInBedCnt": sleepInfoReport.notInBedCnt,          //"0"
                    "totalNotInBedTime": sleepInfoReport.totalNotInBedTime,         //1234
                    "message": sleepInfoReport.message,                 //"睡眠資訊"
                    //"tenantName": sleepInfoReport.tenantName,              //"周阿德"
                    "patientId" : sleepInfoReport.patientId                //"5a9d013ea5012409ca68e1f5"                    
                },(err,response) => {
                    if(err){reject(err)}
                    this.transactionNumber++;
                    console.log(response)
                    resolve(response)
                })
            } catch (error) {
                console.log(``)
                reject(error)
            }
        })
    }

    public BedStatusUpdate = (bedStatusReport: BedStatusReport) => {
        return new Promise ((resolve,reject) => {
            try {
                console.log(bedStatusReport)
                this.spaceStatusHandler.BedStatusUpdateStub({
                    "transactionNo": this.transactionNumber.toString(),
                    "room": bedStatusReport.room,
                    "type": bedStatusReport.type,                   //"MWG-Sleep-Info"
                    "mattressMac": bedStatusReport.mattressMac,             //"01BA4C1A",
                    "bedStateAlertLevel": bedStatusReport.bedStateAlertLevel,        //"0 - 不警報，一般狀態顯示"
                    "bedState": bedStatusReport.bedState,                  //"15 - 側躺"
                    "message": bedStatusReport.message                 //"床墊資訊"                       
                },(err,response) => {
                    if(err){reject(err)}
                    this.transactionNumber++
                    console.log(response)
                    resolve(response)    

                })
            } catch(error) {
                console.log(`fn:BedStatusUpdate ${error}`)
                reject(error)
            }
        })
    }

    public BedSettingUpdate = (bedSettingReport : any) => {
        return new Promise ((resolve,reject) => {
            try{
                console.log(bedSettingReport)
                this.spaceStatusHandler.BedSettingUpdateStub(bedSettingReport,(error,response) => {
                    if(error){reject(error)}
                    this.transactionNumber++
                    console.log(response)
                    resolve(response)
                })

            } catch(error){
                console.log(`fn:BedSettingUpdate ${error}`)
                reject(error)
            }
        })
    }
}

//const smilinnServer = new SmileInnGrpcClient('192.168.69.83',50051)
const smilinnGrpcHandler = new SmileInnGrpcClient(grpcClientIp,grpcClientPort)
exports.grpcOutwardHandler = smilinnGrpcHandler

