"use strict";

const fs = require("fs");
import { EventEmitter } from "events";
const JuboSpaceLogEventEmitter = new EventEmitter();

const deleteLogFile = (path:string) => {
    return new Promise((resolve, reject) => {
        try{
            fs.writeFile(path,'', (err) => {
                resolve(console.log("erase complete"));
            });            
        } catch (error){
            reject(console.log("err: "+error));
        }
    });    
}

const writeJuboSpaceLog = (dirUrl:string,data:any) => {
    return new Promise((resolve, reject) => {
        try{
            let dataToWrite = JSON.stringify(data);
            fs.writeFile(dirUrl,dataToWrite,(err) => {
                if(err){
                    console.log(err);
                    reject(err);
                }
                console.log(dataToWrite);
                //fs.readFile for checking
                fs.readFile(dirUrl,'utf-8',(err,dataRead) => {
                    if(err){
                        console.log(err);
                        reject(err);
                    }
                    if(dataRead === dataToWrite){
                        resolve(dataRead);
                    } else {
                        console.log("wtf" + dataRead);
                        //throw new error
                    }
                });
            });
        } catch (error){
            console.log("fn: writeJuboSpaceLog: "+error);
            reject(error);
        }
    });
}

const readJuboSpaceLog = (dirUrl:string) => {
    return new Promise((resolve, reject) => {
        try{
            //fs.readFile for checking
            fs.readFile(dirUrl,'utf-8',(err,dataRead) => {
                if(err){
                    console.log(err);
                    reject(err);
                }
                    resolve(dataRead);
            });            
        } catch (error){
            console.log("fn: readJuboSpaceLog: "+error);
            reject(error);
        }
    });
}

exports.deleteLogFile = deleteLogFile;
exports.writeJuboSpaceLog = writeJuboSpaceLog;
exports.readJuboSpaceLog = readJuboSpaceLog;


