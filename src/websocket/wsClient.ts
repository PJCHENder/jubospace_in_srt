//local test client
export{};

const ws = require('ws');
const utils = require('./../utils/utilities')

import { WsConnectToken } from './../stateMachine/myHealthApp/interfaces/myHealthAppInterface'

let patientId = '5ed5f57439baab1d94cdb495'
let deviceId = 'BFE6211E-B112-4819-3A5C-85F214FC5ED4'

let iPadConnect :WsConnectToken= {
    "uid": '',
    "deviceId": deviceId,
    "patientId": patientId,
    "isAlive": false,
    "ipAddress": "",
    "keepAliveTimeout": ""
}

setTimeout(()=>{

    const wsClient = new ws('ws://localhost:8888');
    const noop = () => {}
    function heartbeat() {
        //clearTimeout(this.pingTimeout);
        wsClient.ping(noop);
        //console.log(this.pingTimeout)
    
      }
    
    wsClient.onopen = () => {
        heartbeat();
            
        setTimeout(() => {
            //wsClient.send('/getchannellist')
        },3000)
    }
    
    wsClient.onerror = (error) => {
        console.log(`WebSocket error: ${error}`)
        heartbeat();
    }
    
    wsClient.onmessage = (e) => {
        console.log(e.data)
        let message:string = e.data
        let head = message.split("=")[0]
        let body = message.split("=")[1]
        
        if(head === '/connectToken'){
            
            let connectToken = JSON.parse(utils.removeByteOrderMark(body))
            console.log(connectToken)
            iPadConnect.uid = connectToken.uid
            iPadConnect.isAlive = connectToken.isAlive
            iPadConnect.ipAddress = connectToken.ipAddress
            iPadConnect.keepAliveTimeout = connectToken.keepAliveTimeout
            console.log(iPadConnect)
            wsClient.send('/connectResp='+JSON.stringify(iPadConnect))
        } else {
            
        }
    }
    
    wsClient.on('ping',function(){
        console.log('ping-ed')
        //wsClient.pong(noop);
    });
    wsClient.on('pong',function(){
        console.log('pong-ed')
        //heartbeat;
    });    
    wsClient.on('close',function clear(){
        //clearTimeout(this.pingTimeout);
    })
    
    let i=0;
    let j=0;
    setInterval(function(){
        console.log("Now:"+new Date().toUTCString()+']');
        if(j==0){
        //    wsClient.send('/aircon=19')
            j=1
        } else if (j==1){
        //    wsClient.send('/tvcmd=philipOnOff')
            j=2
        } else if(j==2){
            //wsClient.send('/updatedevicelist='+JSON.stringify(deviceLists))
            j=0            
        }
        else if(j==3){
            //wsClient.send('/iPadPanicButton=pressed')
            j=4            
        }
        else if(j==4){
           // wsClient.send('/iPadPanicButton=cleared')
            j=0            
        }

        if(i==0){
            
            wsClient.send('/'+deviceId+'#panicButton=pressed')
            i=1;
        } else if (i==1){
            i=2;
            
           wsClient.send('/'+deviceId+'#panicButton=cleared')
        } else if (i==2){
            i=3;
            
            wsClient.send('/'+deviceId+'#lightControl={"scene":"reading"}')
        } else if (i==3){
            i=4;
            
            wsClient.send('/'+deviceId+'#systemInfo={"batteryLevel":"94"}')
        } else if (i==4){
            i=0;
            
            wsClient.send('/'+deviceId+'#lightControl={"scene":"allOn"}')
        } else if (i==5){
            i=6;
            
            //wsClient.send('/lightmode=allOff')
        } else if (i==6){
            i=7;
            
            //wsClient.send('/lightmode=emergencyOn')
        } else if (i==7){
            i=0;
            
            //wsClient.send('/lightmode=allOff')
        }
    },3000);
},3000)
