'use strict';

let roomTemperature=26
let PhilioIniting=false;
const fs = require('fs');
const path = require('path');
const commonEvent = require('./commonEvent').commonEmitter
const wsServer = require("./../websocket/wsServer");
const Philio = require('./../philio/philio');


const commonListener = (roomNumber,
                        philioGateway,
                        iPadController,
                        PhilioController,
                        philioDeviceListUpdate,
                        philioChannelListUpdate,
                        SmileInnGrpc) => {
    
    commonEvent.on('philioRequestError', () => {
        //PhilioIniting=true;
        Philio.commonEventMassacre()
        commonEvent.emit('retry-init')
    })    
    
    commonEvent.on('endPhilioInit', () => {
        //PhilioIniting=false;
    })

    commonEvent.on('pirOn',(event) => {
        for(let motionSensor of PhilioController.pirSense){
            if(motionSensor.uid === parseInt(event.uid)){            
                motionSensor.motionDetected(event)
            }
        }
    })

    commonEvent.on('pirOff',(event) => {
        for(let motionSensor of PhilioController.pirSense){
            if(motionSensor.uid === parseInt(event.uid)){
                motionSensor.noMotionTimeout(event)
            }
        }
    })

    commonEvent.on('tamper', async (event) => {
        //console.log('@ tamper: ');
        // console.log(event);
        for(let philioDevice of philioDeviceListUpdate ){
    
            if(philioDevice.uid === parseInt(event.uid)){
                // console.log(philioDevice)
                if(philioDevice.tamperStatus === 'noTampered'){
                    philioDevice.tamperStatus = 'isTampered'
                    let now = new Date().getTime();
                    philioDevice.tamperTimeStamp = now;
                    await SmileInnGrpc.EquipmentStatusUpdate({
                        "transactionNo": event.sequence,
                        "name": 'smileInnReport',
                        "room": roomNumber,
                        "type": 'TamperedAlert',
                        "statusUpdateTime": now,
                        "powerLevel": event.battery,
                        "productId": philioDevice.productId,
                        "functype": philioDevice.description,
                        "userDefineName": philioDevice.userDefineName,
                        "location": philioDevice.location,
                        "message": '設備移位'              
                    })
                }
            }
        }  
    })

    commonEvent.on('tamperClear', async (device) => {
        try{
            device.tamperStatus = 'noTampered'
            let now = new Date().getTime();
            await SmileInnGrpc.EquipmentStatusUpdate({
                "transactionNo": 0,
                "name": 'smileInnReport',
                "room": roomNumber,
                "type": 'TamperedTimeout',
                "statusUpdateTime": now,
                "powerLevel": device.powerLevel,
                "productId": device.productId,
                "functype": device.description,
                "userDefineName": device.userDefineName,
                "location": device.location,
                "message": '移位解除'              
            })

        } catch( error ) {

        }
    })

    commonEvent.on('batteryLow', async (event) => {
        try{       
            // console.log('@ battery low: ');
            //console.log(event);
            for(let philioDevice of philioDeviceListUpdate ){
                if(philioDevice.uid === parseInt(event.uid)){
                    if(philioDevice.powerStatus != 'batteryLow'){
                        philioDevice.powerStatus = 'batteryLow'
                        let now = new Date().getTime();
                        await SmileInnGrpc.EquipmentStatusUpdate({
                            "transactionNo": event.sequence,
                            "name": 'smileInnReport',
                            "room": roomNumber,
                            "type": 'BatteryLowStatus',
                            "statusUpdateTime": now,
                            "powerLevel": event.battery,
                            "productId": philioDevice.productId,
                            "functype": philioDevice.description,
                            "userDefineName": philioDevice.userDefineName,
                            "location": philioDevice.location,
                            "message": '電量不足'              
                        })
                    }
                }
            }  
        } catch(error){

        } 
    })

    commonEvent.on('ipad-panic-button-pressed', (event)=> {
        try{
            let iPadButtons = iPadController.panicButton
            for(let iPadButton of iPadButtons){
                let panicStatus = iPadButton.PanicMachine.value;
                if(panicStatus == 'noPanic'){
                    let now = new Date().getTime();
                    iPadButton.ButtonPressed({
                        "basicValue": '255',
                        "eventCode": '5002',
                        "battery": '255',
                        "dataUnit": '5',
                        "sensorValue": '0',
                        "timeStamp": now,
                        "funcType": 'iPadPanicBtn',
                        "uid": '999',
                        "funcName": 'iOS Panic Button',
                        "sequence": '9999'   
                    })
                } else if(panicStatus == 'isPanic'){
                
                } else {
                    panicStatus == 'noPanic'
                }
            }

        } catch (error) {
            console.log("fn: commonEvent.on 'panic-button-pressed '"+ error)
        }
    })

    commonEvent.on('ipad-panic-button-cleared', (event) => {
        try{
            let iPadButtons = iPadController.panicButton
            for(let iPadButton of iPadButtons){
                let panicStatus = iPadButton.PanicMachine.value;
                if(panicStatus == 'noPanic'){

                } else if(panicStatus == 'isPanic'){
                    let now = new Date().getTime();
                    iPadButton.ButtonCleared({
                        "basicValue": '0',
                        "eventCode": '5002',
                        "battery": '255',
                        "dataUnit": '5',
                        "sensorValue": '0',
                        "timeStamp": now,
                        "funcType": 'iPadPanicBtn',
                        "uid": '999',
                        "funcName": 'iOS Panic Button',
                        "sequence": '9999'                    
                    })
                } else {
                    panicStatus == 'isPanic'
                }
            }
        } catch (error) {
            console.log("fn: commonEvent.on 'panic-button-clear '"+ error)
        }
    })

    commonEvent.on('ipadBattery', async (event) => {
        let batteryInfo = event.split('=')[1];
        
        let iPadButtons = iPadController.panicButton
        for(let iPadButton of iPadButtons){
            iPadButton.powerLevel = Math.round(parseFloat(batteryInfo)*100);
            // console.log(iPadButton)
            // console.log(iPadButton.powerLevel)
            let now = new Date().getTime();
            if(iPadButton.powerLevel <= 10){
                if(iPadButton.powerStatus === 'normal'){
                    iPadButton.powerStatus = 'low'

                    await SmileInnGrpc.EquipmentStatusUpdate({
                        "transactionNo": 9999,
                        "name": 'smileInnReport',
                        "room": roomNumber,
                        "type": 'BatteryLowStatus',
                        "statusUpdateTime": now,
                        "powerLevel": iPadButton.powerLevel,
                        "productId": iPadButton.productId,
                        "functype": iPadButton.functype,
                        "userDefineName": iPadButton.userDefineName,
                        "location": iPadButton.location,
                        "message": '電量不足'              
                    })  
                }
    
            } else {
                if(iPadButton.powerStatus === 'low'){
                    iPadButton.powerStatus = 'normal'

                    await SmileInnGrpc.EquipmentStatusUpdate({
                        "transactionNo": 9999,
                        "name": 'smileInnReport',
                        "room": roomNumber,
                        "type": 'BatteryNormalStatus',
                        "statusUpdateTime": now,
                        "powerLevel": iPadButton.powerLevel,
                        "productId": iPadButton.productId,
                        "functype": iPadButton.functype,
                        "userDefineName": iPadButton.userDefineName,
                        "location": iPadButton.location,
                        "message": '電量正常'              
                    })   
                }        
            }
        }
    })

    commonEvent.on('statusUpdate', (event) => {
        // console.log(event)
        for(let panicButton of PhilioController.panicButton){
            //console.log(panicButton)
            if(panicButton.uid === parseInt(event.uid)){
                if(event.basicValue=='255'){
                    panicButton.ButtonPressed(event)
                } else if(event.basicValue=='0'){
                    panicButton.ButtonCleared(event)
                }
            }
        }
    })

    commonEvent.on('temperature', (event) => {
        //console.log(event)
        if(event.dataUint === '1'){
            roomTemperature = parseInt(event.sensorValue)/10
        } else if (event.dataUnit === '2'){
            roomTemperature = (parseFloat(event.sensorValue)/10-32)*5/9

        }
        
        console.log('/temperature='+roomTemperature.toFixed(1))
        wsServer.wssBroadcast('/temperature='+roomTemperature.toFixed(1))
    })

    commonEvent.on('doorOpen', (event) => {
        console.log('doorOpen')
        console.log(event)
        doLightSceneAuto('allon')
    })

    commonEvent.on('doorClose', (event) => {
        console.log('doorClose')
        console.log(event)  
        doLightSceneAuto('reading')  
    })

    commonEvent.on('morningLightMode',(event) => {
        try{
            if(PhilioIniting){
                console.log("lightScenes=morningLight at PhilioIniting")
            } else {
                doLightSceneAuto('morning')
                // Philio.doLightScene(philioGateway,'morning')
            }
        } catch(error){
            if(error.name === "RequestError"){                    
                commonEvent.emit('philio-reboot')
            }
        }  
    })

    commonEvent.on('readingLightMode',(event) => {
        try{
            if(PhilioIniting){
                console.log("lightScenes=readingLight at PhilioIniting ")
            } else {
                doLightSceneAuto('reading')
                // Philio.doLightScene(philioGateway,'reading')
            }
        } catch(error){
            if(error.name === "RequestError"){                    
                commonEvent.emit('philio-reboot')
            }
        }     
    })

    commonEvent.on('restingLightMode',function(event){
        try{
            if(PhilioIniting){
                console.log("lightScenes=restingLight at PhilioIniting")    
            } else {
                doLightSceneAuto('resting')
            }
        } catch(error){
            if(error.name === "RequestError"){                    
                commonEvent.emit('philio-reboot')
            }
        }     
    })

    commonEvent.on('sleepingLightMode',function(event){
        try{
            if(PhilioIniting){
                console.log("lightScenes=sleepingLight at PhilioIniting")    
            } else {
                doLightSceneAuto('sleeping')
                //Philio.doLightScene(philioGateway,'sleeping')
            }
        } catch(error){
            if(error.name === "RequestError"){                    
                commonEvent.emit('philio-reboot')
            }
        }              
    })

    commonEvent.on('allOnLightMode',function(event){
        try{
            if(PhilioIniting){
                console.log("lightScenes=allOn at PhilioIniting")    
            } else {
                doLightSceneAuto('allon')
                //Philio.doLightScene(philioGateway,'allon')
            }
        } catch(error){
            if(error.name === "RequestError"){                    
                commonEvent.emit('philio-reboot')
            }
        }        
    })

    commonEvent.on('allOffLightMode',function(event){
        try{
            if(PhilioIniting){
                console.log("lightScenes=allOff at PhilioIniting")
            } else {
                doLightSceneAuto('alloff')
                //Philio.SwitchAllOff(philioGateway)
            }
        } catch(error){
            if(error.name === "RequestError"){                    
                commonEvent.emit('philio-reboot')
            }            
        }          
    })

    commonEvent.on('emergencyOnLightMode',function(event){
        try{
            if(PhilioIniting){
                console.log('lightScenes=emergencyOnLightMode at PhilioIniting')    
            } else {
                //Philio.PhilioLightControl(lightScenes.emergencyOn);
            }
        } catch(error){
            if(error.name === "RequestError"){                    
                commonEvent.emit('philio-reboot')
            } 
        }              
    })
    commonEvent.on('done-light-scene',(stringResp) =>{
        let resp = '/ack='+stringResp
        wsServer.wssBroadcast(resp)
    })
    let test = 0
    commonEvent.on('testing', () => {
        try{

        } catch (error)
        {

        }        

    })

    const doLightSceneAuto = async (sceneName:string) => {
        const MAX_RETRIES = 10;
        for(let i=0; i<=MAX_RETRIES; i++){
            try{
                let sceneResp = await Philio.doLightScene(philioGateway,sceneName)
                if(sceneResp.split('<br>')[0]===sceneName && sceneResp.split('<br>')[1] === 'doscene done'){
                    
                    i=MAX_RETRIES;
                    let stringResp = 'lightmode-'+sceneName
                    //commonEvent.emit('done-light-scene',stringResp)
                }
            } catch (error){
                const timeout = i*1000;
                console.log('Waiting', timeout, 'msec');
                await wait(timeout)
                console.log("doLightSceneAuto Retrying", error.message, i);
            }
        }
    }

    const wait = (timeout: number) => {
        return new Promise((resolve,reject) => {
            setTimeout( ()=> {
                resolve()
            },timeout)
        })
    }



}

module.exports = 
    commonListener;
