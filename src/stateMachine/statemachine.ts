'use strict';

const createStateMachine = (stateMachineDefinition) => {
    const machine = {
        value: stateMachineDefinition.initState,
        transitions(currentState,event,data) {
            const currentStateDefinition = stateMachineDefinition[currentState];
            const destinationTransition = currentStateDefinition.transitions[event];
            if(!destinationTransition){
                return ;
            }
            const destinationState = destinationTransition.target;
            const destinationStateDefinition =
              stateMachineDefinition[destinationState];
            
            destinationTransition.action(data);
            currentStateDefinition.actions.onExit(data);
            destinationStateDefinition.actions.onEnter(data);

            machine.value = destinationState;

            return machine.value;
        }
    }
    return machine;
}

exports.createStateMachine = createStateMachine;
