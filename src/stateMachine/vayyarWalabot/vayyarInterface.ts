import { GenericPresenceDetector,GenericFallDetector,GenericWIFIDeviceStatus } from './../../interfaces/genericInterface'

export interface VayyarPresenceMessage {
    "presenceDetected": boolean;
    "roomPresenceIndication": number;
    "presenceTargetType": number;
    "timestamp": number;
    "presenceRegionMap": any;       //empty object
}

export interface VayyarFallMessage {
    "endTimestamp": number;
    "status": string; 
    "isSimulated": boolean;
    "statusUpdateTimestamp": number;   
    "timestamp": number;
    "type": string;
    "isLearning": boolean;
    "isSilent": boolean;
}

export interface VayyarDeviceState {
    "timestamp": number;
    "temperature": number;
    "humidity": number;
    "status": string;
    "batteryTemperature": number;
    "batteryLevel": number;
    "upTime": number;
    "wifiState": VayyarWifiState;
    "storageState": VayyarStorageState;
    "memoryUsage": number;
    "cpuUsage": number;
}

type VayyarWifiState = {
    "ssid": string;
    "signalLevel": number;
    "frequency": number;
    "linkSpeed": number;
    "rssi": number;
}

type VayyarStorageState = {
    "totalStorageMBytes": number;
    "freeStorageMBytes": number;  
}
//
