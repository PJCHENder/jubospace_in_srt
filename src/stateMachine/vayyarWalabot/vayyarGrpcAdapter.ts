'use strict'

const grpcHandler = require('./../../grpc/juboSpaceDeviceClient').grpcDeviceClientHandler

const vayyarApi = require('./vayyarGrpcAdapterApi')
const utils = require('./../../utils/utilities')

const fallDetectGrpcAdapter = async (message : any) => {
    try {
        //console.log(message)
        let messageTemp = vayyarApi.vayyarFallAdapterLogic
                .find((x: { status: string; }) => x.status === message.fallReport.status).message
        
        if(messageTemp === undefined){messageTemp = '偵測系統初始'}

        await grpcHandler.DeviceEventGrpcUpdate({
            "transactionNo": 0,
            "NISPatientId": message.tenant.id_number,
            "boxDeviceId": message.deviceInfo.vayyarWalabot.boxDeviceId,
            "name": "Vayyar Walabot Home",
            "location": message.deviceInfo.vayyarWalabot.location,
            "room": message.roomNum,
            "event": message.fallReport.status,
            "message": message.message,
            "occurredAt": utils.formatDate(message.fallReport.statusUpdateTimestamp,"Y-m-d H:i:s")
        })

    } catch (error) {
        console.log(`fn: processFallDetectGrpc ${error}`)
    }
 }
 
 const fallCancelGrpcAdapter = async (message : any) => {
    try {
        //console.log(message)
        let messageTemp = vayyarApi.vayyarFallAdapterLogic
                .find((x: { status: string; }) => x.status === message.fallReport.status).message
        
        if(messageTemp === undefined){messageTemp = '偵測系統初始'}

        await grpcHandler.DeviceEventGrpcUpdate({
            "transactionNo": 0,
            "NISPatientId": message.tenant.id_number,
            "boxDeviceId": message.deviceInfo.vayyarWalabot.boxDeviceId,
            "name": "Vayyar Walabot Home",
            "location": message.deviceInfo.vayyarWalabot.location,
            "room": message.roomNum,
            "event": message.fallReport.status,
            "message":message.message,
            "occurredAt": utils.formatDate(message.fallReport.statusUpdateTimestamp,"Y-m-d H:i:s")
        })

    } catch (error) {
        console.log(`fn: fallCancelGrpcAdapter ${error}`)
    }
 }

 const presenceDetectGrpcAdapter = async (message : any) => {
     try {
         //console.log(message)
        await grpcHandler.DeviceEventGrpcUpdate({
            "transactionNo": 0,
            "NISPatientId": message.tenant.id_number,
            "boxDeviceId": message.deviceInfo.vayyarWalabot.boxDeviceId,
            "name": "Vayyar Walabot Home",
            "location": message.deviceInfo.vayyarWalabot.location,
            "room": message.roomNum,
            "event": "Presence Detected",
            "message":"觸發活動感知",
            "occurredAt": utils.formatDate(message.presenceReport.timestamp,"Y-m-d H:i:s")
        })

     } catch (error) {
        console.log(`fn:presenceDetectGrpcAdapter ${error}`)
     }
}

const presenceCancelGrpcAdapter = async (message : any) => {
    try {
        //console.log(message)
        await grpcHandler.DeviceEventGrpcUpdate({
            "transactionNo": 0,
            "NISPatientId": message.tenant.id_number,
            "boxDeviceId": message.deviceInfo.vayyarWalabot.boxDeviceId,
            "name": "Vayyar Walabot Home",
            "location": message.deviceInfo.vayyarWalabot.location,
            "room": message.roomNum,
            "event": "Presence Canceled",
            "message":"解除活動感知",
            "occurredAt": utils.formatDate(message.presenceReport.timestamp,"Y-m-d H:i:s")
        })

    } catch (error) {
        console.log(`fn:presenceCancelGrpcAdapter`)
    }
}


 exports.fallDetectGrpcAdapter = fallDetectGrpcAdapter;
 exports.fallCancelGrpcAdapter = fallCancelGrpcAdapter;

 exports.presenceDetectGrpcAdapter = presenceDetectGrpcAdapter;
 exports.presenceCancelGrpcAdapter = presenceCancelGrpcAdapter;