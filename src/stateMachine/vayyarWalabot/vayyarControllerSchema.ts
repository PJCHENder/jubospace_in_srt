

module.exports = {
    initState: 'waitDevice',
    waitDevice: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            deviceFound: {
                target: 'deviceConnect',
                action(message) {
                    console.log('waitDevice ==> deviceConnect')
                }
            },
            deivceDisconnect: {
                target: 'waitDevice',
                action(message) {}
            }                  
        }
    },
    deviceConnect: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            monitorUp: {
                target: 'stateAquired',
                action(message) {
                    console.log('deviceConnect => stateAquired')
                }
            },
            deviceDisconnect: {
                target: 'waitDevice',
                action(message) {}
            }                                                     
        }
    },
    stateAquired: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            presenceUpdate: {
                target: 'systemOperational',
                action(message) {
                    console.log('stateAquired => systemOperational')
                }
            },
            deviceDisconnect: {
                target: 'waitDevice',
                action(message) {}
            }                                                     
        }
    },
    systemOperational: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            deviceDisconnect: {
                target: 'waitDevice',
                action(message) {}
            }                                                     
        }
    },
    systemError: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            errorResolvedRestart: {
                target: 'waitDevice',
                action(message) {}
            }                                                     
        }
    },          
}