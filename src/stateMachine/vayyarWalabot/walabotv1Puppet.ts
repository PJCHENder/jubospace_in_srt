'use strict';

import { VayyarFallMessage, 
        VayyarPresenceMessage, 
        VayyarDeviceState } from './vayyarInterface'

import { EventEmitter } from 'events';

const puppeteer = require('puppeteer');
const utils = require('./../../utils/utilities')

const wait = require('util').promisify(setTimeout)
const ASYNC_TIMEOUT_200MS = 200
const ASYNC_TIMEOUT_400MS = 400
const ASYNC_TIMEOUT_600MS = 600
const ASYNC_TIMEOUT_800MS = 800

let WalabotUrl = 'http://localhost:7777/raw-data'
let browser;
let page;

interface Walabot {
    walabotId: string;
    deviceIsReady: boolean;
    deviceState: VayyarDeviceState;
    fallDetector: VayyarFallMessage;
    presenceDetector: VayyarPresenceMessage;

    on(event: 'isReady', listener: () => void): this;
    on(event: 'restart', listener: () => void): this;

    setUrl (url:string);
    getState(deviceId:string);
    getPresenceStatus(deviceId:string);
    getFallStatus(deviceId:string);
} 

export class WalabotV1 extends EventEmitter implements Walabot {
    walabotId: string
    deviceIsReady: boolean
    "deviceState": VayyarDeviceState
    "fallDetector": VayyarFallMessage
    "presenceDetector": VayyarPresenceMessage
    
    constructor(){
        super()
        this.walabotId=''
        this.deviceIsReady=false
        this.puppetInitWalabot()
        .catch((error) => {
            console.log('~~~~~~~~~~~~~~~~~~~~~ puppet error ~~~~~~~~~~~~~~`')
        })
        .then(()=>{
            this.deviceIsReady=true
            this.emit('isReady')
        })
    }

    public setUrl = (url:string) => {
        WalabotUrl = url
    }

    public setWalabotId = (_id:string) => {
        this.walabotId=_id
    }

    public getId = async (deviceId:string) => {
        try {
            await page.waitForSelector('.mat-card-title')
            let title = await page.evaluate(() => {
              return document.querySelector('.mat-card-title').textContent
            })   
            if(title.includes('id_')){
                this.setWalabotId(title)
            }
            return title
        } catch (error){
            throw new Error('getId _Id not found')
        }
    }

    public getState = async (deviceId:string) => {
        try{
            await page.waitForSelector('.mat-card-title')
            let title = await page.evaluate(() => {
              return document.querySelector('.mat-card-title').textContent
            })   
            
            if(this.walabotId == title){
                //console.log(`${title} getState`)
              await page.waitForFunction('document.querySelector(".mat-card-content > p").innerText.includes("State")')
              let stateContent = await page.evaluate(()=>{
                return document.querySelector('.mat-card-content > p:nth-child(1)').textContent
              })
              if(stateContent){
                let stateContentStr = utils.removeByteOrderMark(stateContent)
                if(/({|\})+/gm.test(stateContentStr)){
                    return(JSON.parse('{'+stateContentStr+'}'))
                } else {
                    return(JSON.parse('{'+stateContentStr+'{}}'))
                }
                
              } else {
                throw new Error('getState JSON not found')
              }
            }  
        } catch (error) {
            console.log(`fn: getWalabotState => ${error}`)
            if(error.name.includes('Protocol error')){
                await browser.close()
                this.deviceIsReady = false;
                this.emit('restart')
                 this.puppetInitWalabot()
                 .then(()=>{
                     this.emit('isReady')
                     this.deviceIsReady=true
                 })                
            } else if(error.name.includes('TimeoutError')){
                return error.message
            } else {
                return error.message
            }
        }
    }

    public getPresenceStatus = async (deviceId:string) => {
        try{
            await page.waitForSelector('.mat-card-title')
            let title = await page.evaluate(() => {
              return document.querySelector('.mat-card-title').textContent
            })   
             
            if(this.walabotId == title){
                //console.log(`${title} getPresenceStatus`) 
              await page.waitForFunction('document.querySelector(".mat-card-content > p").innerText.includes("State")')
              let stateContent = await page.evaluate(()=>{
                return document.querySelector('.mat-card-content > p:nth-child(2)').textContent
              })
              if(stateContent){
                let stateContentStr = utils.removeByteOrderMark(stateContent)
                if(/({|\})+/gm.test(stateContentStr)){
                    return(JSON.parse('{'+stateContentStr+'}'))
                } else {
                    //console.log(stateContentStr)
                    return(JSON.parse('{'+stateContentStr+'{}}'))
                }                
              } else {
                  throw new Error('getPresenceStatus JSON not found')
              }

            }  
        } catch (error) {
            console.log(`fn: getPresenceStatus => ${error}`)
            
            if(error.name.includes('Protocol error')){
                await browser.close()
                this.deviceIsReady = false;
                await wait(2000);
                this.puppetInitWalabot()
                    .then(()=>{
                        this.emit('isReady')
                        this.deviceIsReady=true
                    })                
            } else if(error.name.includes('TimeoutError')){
                return error.message
            } else {
                return error.message
            }
        }
    }

    public getFallStatus = async (deviceId:string) => {
        let jsonResp:any;
        try{
            await page.waitForSelector('.mat-card-title')
            let title = await page.evaluate(() => {
              return document.querySelector('.mat-card-title').textContent
            })   
             
            if(this.walabotId == title){
                //console.log(`${title} getFallStatus `) 
                await page.waitForFunction('document.querySelector(".mat-card-content > p").innerText.includes("State")')
                let stateContent = await page.evaluate(()=>{
                    return document.querySelector('.mat-card-content > p:nth-child(3)').textContent
                })

                if(stateContent){
                    let stateContentStr = utils.removeByteOrderMark(stateContent)
                    
                    if(/({|\})+/gm.test(stateContentStr)){
                        return (JSON.parse('{'+stateContentStr+'}'))
                    } else {
                        return (JSON.parse('{'+stateContentStr+'{}}'))
                    }
                    
                } else {
                    throw new Error('getFallStatus JSON not found')
                }
            }  
        } catch (error) {
            console.log(`fn: getFallStatus => ${error}`)
            
            if(error.name.includes('Protocol error')){
                this.deviceIsReady = false;
                await browser.close()
                this.emit('restart')
                 this.puppetInitWalabot()
                 .then(()=>{
                     this.emit('isReady')
                     this.deviceIsReady=true
                 })
                jsonResp = {}   
            } else if(error.name.includes('TimeoutError')){
                return error.message
            }
        }
    }

    public puppetInitWalabot = async () => {
        const MAX_RETRIES = 10;
        for(let i=0;i<=MAX_RETRIES;i++){
            try {
                browser = await puppeteer.launch({
                    userDataDir: "~/Library/Application Support/Google/Chrome/Profile 1",
                    headless: false,        // for debugging only
                    ignoreHTTPSErrors: true // This happens when you use a self signed certificate locally
                })
                await this.wait(2000)
                page = await browser.newPage()
                await page.setUserAgent("Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10136");
                await page.setViewport({ width: 1280, height: 800 })
                await page.setDefaultNavigationTimeout(0)
                await page.goto(WalabotUrl, {waitUntil: 'networkidle2'})
        
                console.log('Walabot ===> Waiting For Devices to Connect') 
        
                await page.waitForSelector('.mat-card-title')
                let title = await page.evaluate(() => {
                  return document.querySelector('.mat-card-title').textContent
                })   
                if(title.includes('id_')){
                    this.setWalabotId(title)
                }
                return title
        
            } catch (error){
                const timeout = 3000;
                browser.close()
                console.log(`fn: puppetInitWalabot => ${error}`)
                console.log('Puppeteer retry:'+((timeout/1000)+1)+'time')
                await this.wait(timeout)
                
            }
        }
    }

    private wait = (timeout: number) => {
        return new Promise((resolve,reject) => {
            setTimeout( ()=> {
                resolve()
            },timeout)
        })
    }   
}




