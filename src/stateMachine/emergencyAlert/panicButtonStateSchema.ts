module.exports = {
    initState: 'noPanic',
    noPanic: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            iPadPanicPressed: {
                target: 'isPanic',
                action(message) {}
            },
            buttonPanicPressed: {
                target: 'isPanic',
                action(message) {}
            }                    
        }
    },
    isPanic: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            iPadPanicCleared: {
                target: 'noPanic',
                action(message) {}
            },
            buttonPanicCleared: {
                target: 'noPanic',
                action(message) {}
            },
            dashBoardPanicCleared: {
                target: 'noPanic',
                action(message) {}
            },
            timeoutPanicCleared: {
                target: 'noPanic',
                action(message) {}
            }                                        
        }
    }
}