'use strict'

const panicButtonHandler = require('./../../grpc/juboSpaceDeviceClient').grpcDeviceClientHandler
const utils = require('./../../utils/utilities')
import { iPadDeviceDbInterface,
    genericSystemInfo } from './../myHealthApp/interfaces/myHealthAppInterface'

const panicButtonPressedGrpcAdapter = async (deviceInterface: iPadDeviceDbInterface | any,) => {
    try{
        //console.log(deviceInterface)
        return await panicButtonHandler.DeviceEventGrpcUpdate({
            "NISPatientId": deviceInterface.patientId,
            "boxDeviceId": deviceInterface.boxDeviceId,
            "name": deviceInterface.deviceName,
            "location": deviceInterface.location.facility + ' - ' + deviceInterface.location.location,
            "room": deviceInterface.location.roomNum,
            "event": 'Panic Button Pressed',
            "message": '緊急按鈕觸發',
            "occurredAt": utils.formatDate(new Date().getTime(),"Y-m-d H:i:s")
        })
    } catch (error) {
        console.log(`fn: panicButtonPressedGrpcAdapter ${error}`)
    }
}

const panicButtonClearedGrpcAdapter = async (deviceInterface: iPadDeviceDbInterface | any) => {
    try{
        console.log(deviceInterface)
        return await panicButtonHandler.DeviceEventGrpcUpdate({
            "NISPatientId": deviceInterface.patientId,
            "boxDeviceId": deviceInterface.boxDeviceId,
            "name": deviceInterface.deviceName,
            "location": deviceInterface.location.facility + ' - ' + deviceInterface.location.location,
            "room": deviceInterface.location.roomNum,
            "event": 'Panic Button Cleared',
            "message": '緊急按鈕解除',
            "occurredAt": utils.formatDate(new Date().getTime(),"Y-m-d H:i:s")
        })
    } catch (error) {
        console.log(`fn: panicButtonClearedGrpcAdapter ${error}`)
    }
}

exports.panicButtonPressedGrpcAdapter = panicButtonPressedGrpcAdapter
exports.panicButtonClearedGrpcAdapter = panicButtonClearedGrpcAdapter

