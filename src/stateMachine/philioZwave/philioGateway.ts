'use strict';

const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const fsm = require('./../statemachine')
//const FsmController = require('./../stateMachine/FsmController')
const systemDefinition = require('./systemControl/philioSystemSchema')
const systemController = require('./systemControl/philioSystemControl')
const sceneController = require('./sceneControl/philioSceneControl')
const apiHandler = require('./philioApi/philioApiHandler')

const philioEvent = require('./../../commonEvent/commonEvent').commonEmitter

const SIX_SECOND_COUNTER = 6000
const ONE_MINUTE_COUNTER = 10

import { EventEmitter } from "events";
    
import { GenericNetworkDevice, GenericSpaceInfo } from './../../interfaces/genericInterface'


type GenericConnectAuth = {
    "user": string;
    "pass": string;
    "sendImmediately": boolean;
}
class PhilioGateway extends EventEmitter {

    initPhilio : any
    
    fsMachine : any
    gatewayState : string
    deviceEventSequence : number
    timerCounter : number

    philioIfConfig : any
    timerHandler : any
    philioReady : boolean
    Philio : any
    serverIfConfig : GenericNetworkDevice
    deviceList: any
    sceneList: any
    macroList: any
    deviceNickname: any
    deviceChannel: any
    roomNum: number
    connectOption: any
    spaceDeviceDb: any

    constructor (spaceInfo : GenericSpaceInfo, spaceDeviceDb: any, connectAuth ?: GenericConnectAuth | undefined){
        super()
        this.deviceEventSequence = 1;
        this.timerCounter = 0;
        this.connectOption = {
            uri: '',
            port: 80,
            method: 'GET',
            auth: {
                'user': 'admin', 
                'pass': '888888',
                'sendImmediately': true
            }
        }
        this.spaceDeviceDb = spaceDeviceDb
        this.philioReady = false
        this.roomNum = spaceInfo.roomNum
        this.serverIfConfig = spaceInfo.netInfo
        this.fsMachine = fsm.createStateMachine(systemDefinition)
        this.gatewayState = this.fsMachine.value
        this.initPhilio = this.philioInit()
    }
    public philioInit = async () => {
        try{
            console.log('----entering philioInit-----')
            this.philioIfConfig = await systemController.findPhilioGateway()
            //console.log(this.philioIfConfig)
            let ifaceResp = await apiHandler.getInterface(this.philioIfConfig)
            this.Philio = await systemController.createPhilioGateway(this.philioIfConfig,this.connectOption,ifaceResp)
            //console.log(this.Philio)
            if(this.Philio){
                console.log('----------- Philio Initializing -----------') 
                await systemController.initNetSetting(this.Philio, this.serverIfConfig)
                let philioJsonResp = await apiHandler.getDevices(this.Philio)
                // console.log(philioJsonResp)
                let philioDeviceList = await systemController.getDeviceList(philioJsonResp)
                //console.log(philioDeviceList)
                this.deviceList = await systemController.updateDeviceFromFile(philioDeviceList)        
                let philioChannelList = await systemController.createChannelList(philioJsonResp);
                //console.log(JSON.stringify(philioChannelList))
                this.deviceChannel = await systemController.updateChannelFromFile(philioChannelList)        
                // console.log(philioChannelListUpdate)
                systemController.procJsonGetAll(this.Philio)
                                .then((result)=>{
                                    this.sceneList = result.scenes
                                    this.macroList = result.macros
                                    this.deviceNickname = result.targetNames
                                })                          

                this.gatewayState = this.fsMachine.transitions(this.gatewayState,'deviceInitDone')    

            }
        } catch (error){
            console.log(error);
            //SystemEvent.emit('retry-sequence')
        } finally {
            console.log('done-philioInit-routine')
            this.timerHandler = setInterval(this.timedMethods.bind(this),SIX_SECOND_COUNTER)
        }
    }

    private timedMethods = () => {
        try{
            console.log(this.gatewayState)
            switch(this.gatewayState){
                case 'philioSequence':
                    this.getCurrentSequence ()
                break;

                case 'philioNormal':
                    console.log(this.timerCounter)
                    this.normalPhilioRoutine ()
                    if(this.timerCounter >= ONE_MINUTE_COUNTER){
                        this.gatewayState = this.fsMachine.transitions(this.gatewayState,'philioSequence')
                        this.timerCounter = 0;
                    }                    
                    //this.testing()
                break;

                case 'philioReboot':

                break;

                default:
                    this.gatewayState = 'philioInit'
                break;
            }

            this.timerCounter++;
        } catch (error) {

        }
    }

    private getCurrentSequence = async () => {
        const MAX_RETRIES = 10;
        
        for(let i=0; i<=MAX_RETRIES; i++){
            try{
                let eventLogListInital 
                
                eventLogListInital = await apiHandler.networkConfig(this.Philio, "jsongetevent", this.deviceEventSequence);
                this.deviceEventSequence = await systemController.findCurrentSequence(eventLogListInital);
                if(this.deviceEventSequence != null || this.deviceEventSequence != undefined){
                    i=MAX_RETRIES
                } else {
                    throw new Error('Invalid Log Sequence')
                }
                
            } catch (error){
                const timeout = i*1000;
                console.log('Waiting', timeout, 'msec');
                await wait(timeout)
                console.log("getCurrentSequence Retrying", error.message, i);
    
            } finally {
                if(this.deviceEventSequence>=1){
                    this.gatewayState = this.fsMachine.transitions(this.gatewayState,'acquiredSequence')
                   console.log(`getCurrentSequence ${this.deviceEventSequence}`)
                }
                break;
            }
        }
        console.log("start seq: "+this.deviceEventSequence)  
    }

    private testing = () => {
        console.log('testing codes')
        let test = apiHandler.jsonGetAll(this.Philio)
        sceneController.addScene(this.Philio,this.sceneList,'loveyouall','omg')
        sceneController.addScene(this.Philio,this.sceneList,'lovemetoo','omw',
            {
                "uid": 271,
                "homeId": "E8F8F146",
                "productId": "PAN04-A",
                "functype": "Meter Switch",
                "chid": 1,
                "name": "Wall Switch  CH1",
                "group": "",
                "location": "",
                "description": "In-Wall Switch (500 Series)",
                "userDefineName": "",
                "sceneValMax": 255,
                "sceneValMin": 0
            },
            255
        )
        //sceneController.delScene(this.Philio,'loveyouall')
        sceneController.modifySceneIcon(this.Philio,'loveyouall','beeboo')
        console.log(test)
    }

    private normalPhilioRoutine = () => {
        systemController.eventLogHandler(this.deviceEventSequence,this.Philio)
        .catch((error)=>{
            console.log(`fn: normalPhilioRoutine : ${error}`);
            this.deviceEventSequence=1;
            this.philioReady=false;
            this.gatewayState = this.fsMachine.transitions(this.gatewayState,'philioSequence')
        }).then((result)=>{
            //console.log(result)
            if(result.length >= 1){
                this.deviceEventSequence += result.length
            }     
            philioEvent.emit('philioEventlog',result)
        })
    }

    public iPadGetDeviceList = async () => {
        console.log('hello from iPadGetDeviceList')
    }

    public iPadUpdateDeviceGroup = async () => {

    }

    public iPadCreateScene = async () => {

    }



}

const wait = (timeout: number) => {
    return new Promise((resolve,reject) => {
        setTimeout( ()=> {
            resolve()
        },timeout)
    })
} 


module.exports = PhilioGateway;