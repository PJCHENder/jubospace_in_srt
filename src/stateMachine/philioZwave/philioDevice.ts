'use strict'



const apiHandler = require('./philioApi/philioApiHandler')
import { GenericSpaceInfo } from './../../interfaces/genericInterface'
import { PhilioActuatorInterface, 
        PhilioSensorInterface, 
        PhilioRespDevice } from './interfaces/philioZwaveInterface'



interface PhilioDeviceInterface {
    "spaceInfo": GenericSpaceInfo;
    "spaceDeviceDb": any;
    "device": PhilioActuatorInterface | PhilioSensorInterface;
    updateDeviceParam: () => void;

}

class PhilioDevice implements PhilioDeviceInterface {
    spaceInfo: GenericSpaceInfo;
    spaceDeviceDb: any;
    device: PhilioActuatorInterface | PhilioSensorInterface;

    constructor (philioDeviceRaw:PhilioRespDevice,spaceInfo : GenericSpaceInfo, spaceDeviceDb: any) {
        this.spaceInfo = spaceInfo,
        this.spaceDeviceDb = spaceDeviceDb
        this.device = this.createNewDevice(philioDeviceRaw)

    }

    private createNewDevice = (philioDeviceRaw:PhilioRespDevice) : PhilioActuatorInterface | PhilioSensorInterface => {
        

        return 
    }

    public updateDeviceParam = () => {

    }




}

module.exports = PhilioDevice