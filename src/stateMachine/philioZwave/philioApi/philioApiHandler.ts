"use strict";
const fs = require('fs');
const sleep = require('util').promisify(setTimeout)
const JuboEvent = require('./../../../commonEvent/commonEvent').commonEmitter
const Netlist = require('./../../../netlist/netlist');

const rp = require('request-promise');
const philioApi = require('./philioDeviceApi');

import { NetworkDeviceObject,
            PhilioCmdObject,
            PhilioOptionalCmd,
            PhilioGatewayObject } from "./../interfaces/philioInterface";

const wait = (timeout: number) => {
    return new Promise((resolve,reject) => {
        setTimeout( ()=> {
            resolve()
        },timeout)
    })
}            

const networkConfig = (deviceObject: PhilioGatewayObject, queryCmd: string, val: number | string | undefined) => {
    return new Promise((resolve, reject) => {
        try{
            // console.log(deviceObject);
            if (deviceObject) {
                let connectOptions = deviceObject.connectOption
                let queryResult = "";
                if (queryCmd == "jsongetevent") {
                    
                    if (val === undefined) {
                        queryResult = philioApi.queryCmd.find((x: { cmd: any; }) => x.cmd === queryCmd).query;
                    } else if (typeof val === "string" || typeof val === "number") {
                        queryResult = philioApi.queryCmd.find((x: { cmd: any; }) => x.cmd === queryCmd).query + "=" + val;
                    }
                    //console.log(queryResult)
                } else if (queryCmd == 'reboot'){
                    console.log('~~~~~~~~rebooting comand check~~~~~')
                    queryResult = philioApi.queryCmd.find((x: { cmd: any; }) => x.cmd === queryCmd).query + val;
                    console.log(queryResult)
                } else {
                    queryResult = philioApi.queryCmd.find((x: { cmd: any; }) => x.cmd === queryCmd).query + val;
                }
    
                connectOptions.uri = 'http://' + deviceObject.deviceIp + queryResult;
                //let test = rp(connectOptions);
                //console.log(connectOptions.uri);
                let netConfigOption = rp(connectOptions)
                resolve(netConfigOption);
            }
        } catch (error) {
            console.log(`fn: PhilioNetworkConfig  netConfigOption ${error}`)
            if(error.name==="RequestError"){
                //JuboEvent.emit('philioRequestError')
            }
            reject(error)
        }
    });
}   // end PhilioNetworkConfig

const controlConfig = (deviceObject: PhilioGatewayObject,
    ctrlObj: PhilioCmdObject,
    optional: PhilioOptionalCmd) => {
    return new Promise((resolve, reject) => {

        let jsonCmd: string = '';
        let connectOptions = deviceObject.connectOption
        //console.log(ctrlObj)
        //console.log(optional)
        try {
            switch (ctrlObj.cmd) {

                case 'getconfig':
                    jsonCmd = '{"control":' + JSON.stringify(philioApi.ctrlCmd.find((x: { cmd: string; }) => x.cmd === ctrlObj.cmd)) + '}';
                    //console.log(jsonCmd);
                    connectOptions.uri = 'http://' + deviceObject.deviceIp + '/sdk.cgi?json=' + jsonCmd;
                    break;

                case 'switch':
                    jsonCmd = '{"control":' +
                        JSON.stringify({ ...ctrlObj, ...optional }) +
                        '}';
                    connectOptions.uri = 'http://' + deviceObject.deviceIp + '/sdk.cgi?json=' + jsonCmd;
                    //console.log(connectOptions.uri)
                    break;

                case 'sysrtctime':
                    jsonCmd = '{"control":' + JSON.stringify(philioApi.ctrlCmd.find((x: { cmd: string; }) => x.cmd === ctrlObj.cmd)) + '}';
                    connectOptions.uri = 'http://' + deviceObject.deviceIp + '/sdk.cgi?json=';//+source;
                    break;

                default:
                    //throw new PhiloError("Undefined command.");
                    break;
            }
            let controlPhilioOption = rp(connectOptions)
            
            resolve(controlPhilioOption);
        } catch (error) {
            console.log(`fn:ControlPhilio  controlPhilioOption ${error}`)
            if(error.name==="RequestError"){
                JuboEvent.emit('philioRequestError')
            }   
        }
    });
}   //end controlConfig

const getDevices = (deviceObject: PhilioGatewayObject, deviceUid ?:string): Promise<string> => {
    return new Promise((resolve, reject) => {
        let connectOptions = deviceObject.connectOption
        try {
            if(deviceUid === null || deviceUid === undefined){
                connectOptions.uri = 'http://' + deviceObject.deviceIp + '/sdk.cgi?json={"control":{"cmd":"getdevice"}}';
            } else {
                connectOptions.uri = 'http://' + deviceObject.deviceIp + '/sdk.cgi?json={"control":{"cmd":"getdevice","uid":'+ deviceUid +'}}';
            }
            //console.log(connectOptions.uri);
            let getDeviceOption: string = rp(connectOptions)

            resolve(getDeviceOption);
        } catch (error) {
            console.log(`fn: getDeviceOption PhilioGetDevices ${error}`)
            if(error.name==="RequestError"){
                JuboEvent.emit('philioRequestError')
            }  
        }
    });
}   //end PhilioGetDevices

const getInterface = (deviceObject: NetworkDeviceObject) => {
    return new Promise((resolve, reject) => {
        let connectOptions = deviceObject.connectOption
        try {
            connectOptions.uri = 'http://' + deviceObject.deviceIp + '/sdk.cgi?json={"control":{"cmd":"getinterface","uid":"0"}}';
            let philioConnection = rp(connectOptions)
            resolve(philioConnection);
        } catch (error) {
            console.log(`fn: PhilioGetInterface  philioConnection ${error}`)
            if(error.name==="RequestError"){
                JuboEvent.emit('philioRequestError')
            }   
        }

    });
}   //end PhilioGetDevices


const doLightScene = (deviceObject: PhilioGatewayObject, Scene:string) => {
    return new Promise((resolve, reject) => {
        try{
            if (deviceObject) {
                let connectOptions = deviceObject.connectOption
                //let sceneSelect = ""
                connectOptions.uri = 'http://' + deviceObject.deviceIp + '/network.cgi?doscene=' + Scene;
                console.log(connectOptions.uri)
                let sceneConnect = rp(connectOptions)
                                    .then()
                                    .catch((error) =>{
                                        console.log(`fn:doLightScene  sceneConnect ${error}`)
                                        JuboEvent.emit('philioRequestError')
                                    })

                                    
                resolve(sceneConnect);
            }
        } catch (error) {
            console.log("fn: doLightScene")
            reject(error)
        }
    })
}

const findPhilioGateway = () => {
    return new Promise ((resolve,reject) => {
        try{
            let Gateway = Netlist.find("Philio");
            resolve(Gateway)
        } catch(error){
            reject(error)
        }
    })
}

const jsonDelete = (deviceObject :PhilioGatewayObject,
                        jsonCommandStr : string ) => {
    return new Promise ((resolve, reject) => {
        try{
            if(deviceObject){
                
                let connectOption = deviceObject.connectOption
                connectOption.uri = 'http://' + deviceObject.deviceIp + '/network.cgi?jsondel=' + jsonCommandStr
                let resp = rp(connectOption)
                            .catch((error) => {
                                console.log(`fn:jsonDelete ${error}`)
                                throw new Error('philioRequestError')                                 
                            })
                resolve(resp)
            }
        } catch (error) {
            console.log(`fn: jsonDelete ${error}`)
            reject(error)
        }
    })
}

const jsonSetAll = (deviceObject : PhilioGatewayObject,
                        jsonCommandStr: string ) => {
    return new Promise ((resolve,reject) => {
        try {
            if(deviceObject){
                
                let connectOption = deviceObject.connectOption
                connectOption.uri = 'http://' + deviceObject.deviceIp + '/network.cgi?jsonsetall=' + jsonCommandStr
                console.log(connectOption)
                let resp = rp(connectOption)
                            .catch((error)=>{
                                console.log(`fn:jsonSetAll ${error}`)
                                throw new Error('philioRequestError')  
                            })
                resolve(resp)
            }
        } catch (error) {
            console.log(`fn: jsonSetAll ${error}`)
            reject(error)
        }
    })
}

const jsonGetAll = (deviceObject : PhilioGatewayObject) => {
    return new Promise ((resolve,reject) =>{
        try{
            if(deviceObject){
                let connectOption = deviceObject.connectOption
                connectOption.uri = 'http://' + deviceObject.deviceIp + '/network.cgi?jsongetall'

                let resp = rp(connectOption)
                            .catch((error)=>{
                                console.log(`fn:philioJsonGetAll  respStr ${error}`)
                                throw new Error('philioRequestError')
                            })
                resolve(resp)
            }             
        } catch(error){
            console.log(`fn: jsonGetAll ${error}`)
            reject(error)
        }
    })
}

exports.networkConfig = networkConfig;
exports.controlConfig = controlConfig;

exports.jsonDelete = jsonDelete
exports.jsonGetAll = jsonGetAll
exports.jsonSetAll = jsonSetAll

exports.getInterface = getInterface;
exports.getDevices = getDevices;

exports.doLightScene = doLightScene;

exports.findPhilioGateway = findPhilioGateway;
exports.philioApi=philioApi;

