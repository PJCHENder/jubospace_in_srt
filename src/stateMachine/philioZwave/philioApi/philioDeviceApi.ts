"use strict";
module.exports.productInfo = [
    {
        productId: "0",
        part: 'UNKNOWN',
        product: 'UNKNOWN'
    },
    {
        productId: "3",
        part: 'PAN04',
        product: 'In-Wall Dual Relay 1-way Switch'
    },
    {
        productId: "4",
        part: 'PAN06',
        product: 'In-Wall Dual Relay 1-way Switch'
    },
    {
        productId: "5",
        part: 'PAN07',
        product: 'no datasheet'
    },
    {
        productId: "6",
        part: 'PAN08',
        product: 'Roller Shutter Controller'
    },
    {
        productId: "7",
        part: 'PAN09',
        product: 'Two SPDT switch with meter'
    },
    {
        productId: "12",
        part: 'PST02-A',
        product: 'PIR/Temp/Lumen 3-in-1 (500 Series)'
    },
    {
        productId: "14",
        part: 'PST02-C',
        product: 'Door/Temp/Lumen 3-in-1 (500 Series)'
    },
    {
        productId: "18",
        part: 'PAN04-A',
        product: 'In-Wall Switch (500 Series)'
    },
    {
        productId: "19",
        part: 'PAN06-A',
        product: 'In-Wall Switch (500 Series)'
    },    
    {
        productId: "20",
        part: 'PAN09-A',
        product: 'In-Wall Switch (500 Series)'
    },
    {
        productId: "32",
        part: 'PAT02-B',
        product: 'Temperature Humidity Sensor (500 Series)'
    },
    {
        productId: "39",
        part: 'PSR03-C',
        product: 'Remote Control (500 Series) 1 Key'
    },    
    {
        productId: "40",
        part: 'PAN15',
        product: 'Wall Plug Asia/JP/US'
    },
    {
        productId: "49",
        part: 'PAD02',
        product: 'Dimmer Lamp Holder'
    },    
    {
        productId: "54",
        part: 'PSM08',
        product: 'Door Window Sensor'
    },  
    {
        productId: "81",    //0x51
        part: 'PAD07',
        product: 'In-wall Dimmer, with S1/S2'
    },      
    {
        productId: "110",
        part: 'PAR01',
        product: 'IR Extender (500 Series)'
    },
    {
        productId: "112",
        part: 'PST07',
        product: 'Smart 3-in-1 Sensor - PIR,LUX,TEMP'
    }

];
module.exports.ctrlType = [
    {
        ctrlCode: "23",
        ctrlName: "Switch"
    },
    {
        ctrlCode: "24",
        ctrlName: "Dimmer"
    },
    {
        ctrlCode: "25",
        ctrlName: "Siren"
    },
    {
        ctrlCode: "26",
        ctrlName: "Curtain"
    },
    {
        ctrlCode: "31",
        ctrlName: "Door Lock"
    },
    {
        ctrlCode: "32",
        ctrlName: "Thermostat Fan"
    },
    {
        ctrlCode: "33",
        ctrlName: "Thermostat Mode"
    },
    {
        ctrlCode: "34",
        ctrlName: "Thermostat Temperature"
    },
    {
        ctrlCode: "35",
        ctrlName: "Remote Control"
    },
    {
        ctrlCode: "41",
        ctrlName: "Color Dimmer"
    },
];

module.exports.funcType = [
    {
        funcCode: "0",
        funcName: "Multi-Sensor "
    },
    {
        funcCode: "11",
        funcName: "Temperature Sensor"
    },
    {
        funcCode: "12",
        funcName: "Illumination Sensor"
    },
    {
        funcCode: "13",
        funcName: "Door/Window Sensor"
    },
    {
        funcCode: "14",
        funcName: "PIR Sensor"
    },
    {
        funcCode: "15",
        funcName: "Humidity Sensor"
    },
    {
        funcCode: "16",
        funcName: "GPIO"
    },
    {
        funcCode: "17",
        funcName: "Smoke Sensor"
    },
    {
        funcCode: "18",
        funcName: "CO Sensor"
    },
    {
        funcCode: "19",
        funcName: "CO2 Sensor"
    },
    {
        funcCode: "20",
        funcName: "Flood Sensor"
    },
    {
        funcCode: "21",
        funcName: "Glass Break Sensor"
    },
    {
        funcCode: "22",
        funcName: "Meter Switch"
    },
    {
        funcCode: "23",
        funcName: "Switch"
    },
    {
        funcCode: "24",
        funcName: "Dimmer"
    },
    {
        funcCode: "25",
        funcName: "Siren"
    },
    {
        funcCode: "26",
        funcName: "Curtain"
    },
    {
        funcCode: "27",
        funcName: "Remote"
    },
    {
        funcCode: "28",
        funcName: "Button"
    },
    {
        funcCode: "29",
        funcName: "Meter Sensor"
    },
    {
        funcCode: "30",
        funcName: "Meter Dimmer"
    },
    {
        funcCode: "31",
        funcName: "Door Lock"
    },
    {
        funcCode: "32",
        funcName: "Thermostat Fan"
    },
    {
        funcCode: "33",
        funcName: "Thermostat Mode"
    },
    {
        funcCode: "34",
        funcName: "Thermostat Temperature"
    },
    {
        funcCode: "35",
        funcName: "Remote Control"
    },
    {
        funcCode: "36",
        funcName: "Valve Switch"
    },
    {
        funcCode: "37",
        funcName: "Air Sensor"
    },
    {
        funcCode: "38",
        funcName: "No Definition"
    },
    {
        funcCode: "39",
        funcName: "No Definition"
    },
    {
        funcCode: "40",
        funcName: "UV Sensor"
    },
    {
        funcCode: "41",
        funcName: "Color Dimmer"
    },
    {
        funcCode: "42",
        funcName: "Sunrise(PS)"
    },
    {
        funcCode: "43",
        funcName: "Sunset(PS)"
    },
    {
        funcCode: "43",
        funcName: "Sunset(PS)"
    },
    {
        funcCode: "44",
        funcName: "Scene Number"
    },
];

module.exports.queryCmd = [
    {
        cmd: 'getgateway',
        query: '/network.cgi?getgateway'
    },
    {
        cmd: 'getdnsserver',
        query: '/network.cgi?getdnsserver'
    },
    {
        cmd: 'getnetmask',
        query: '/network.cgi?getnetmask'
    },
    {
        cmd: 'getip',
        query: '/network.cgi?getip'
    },
    {
        cmd: 'getconnectmode',
        query: '/network.cgi?getconnectmode'
    },
    {
        cmd: 'commit',
        query: '/network.cgi?commit'
    },
    {
        cmd: 'wifiscan',
        query: '/network.cgi?wifiscan'
    },
    {
        cmd: 'getssidlist',
        query: '/network.cgi?getssidlist'
    },
    {
        cmd: 'getwifimode',
        query: '/network.cgi?getwifimode'
    },
    {
        cmd: 'getapssid',
        query: '/network.cgi?getapssid'
    },
    {
        cmd: 'getstassid',
        query: '/network.cgi?getstassid'
    },
    {
        cmd: 'getappwd',
        query: '/network.cgi?getappwd'
    },
    {
        cmd: 'getstapwd',
        query: '/network.cgi?getstapwd'
    },
    {
        cmd: 'stalinkstatus',
        query: '/network.cgi?stalinkstatus'
    },
    {
        cmd: 'get_wifi_country_region',
        query: '/network.cgi?get_wifi_country_region'
    },
    {
        cmd: 'wifiscan',
        query: '/network.cgi?wifiscan'
    },
    {
        cmd: 'getwifiapmd5',
        query: '/network.cgi?getwifiapmd5'
    },
    {
        cmd: 'wifiscan',
        query: '/network.cgi?wifiscan'
    },
    {
        cmd: 'getsocketsec',
        query: '/network.cgi?getsocketsec'
    },
    {
        cmd: 'getsocketaddr',
        query: '/network.cgi?getsocketaddr'
    },
    {
        cmd: 'getsocketport',
        query: '/network.cgi?getsocketport'
    },
    {
        cmd: 'socketcommit',
        query: '/network.cgi?socketcommit'
    },
    {
        cmd: 'getswversion',
        query: '/network.cgi?getswversion'
    },
    {
        cmd: 'setconnectmode',
        query: '/network.cgi?setconnectmode=',
        options: {},
        defVal: 'DHCP' //or 'STATIC'
    },
    {
        cmd: 'setstaticip',
        query: '/network.cgi?setstaticip=',
        defVal: '192.168.2.146'
    },
    {
        cmd: 'setnetmask',
        query: '/network.cgi?setnetmask=',
        defVal: '255.255.255.0'
    },
    {
        cmd: 'setgateway',
        query: '/network.cgi?setgateway=',
        defVal: 0
    },
    {
        cmd: 'setdns',
        query: '/network.cgi?setdns=',
        defVal: 0
    },
    {
        cmd: 'setsocketsec',
        query: '/network.cgi?setsocketsec=',
        defVal: 0
    },
    {
        cmd: 'setsocketaddr',
        query: '/network.cgi?setsocketaddr=',
        defVal: 0
    },
    {
        cmd: 'setsocketport',
        query: '/network.cgi?setsocketport=',
        defVal: 0
    },
    {
        cmd: 'jsongetevent',
        query: '/network.cgi?jsongetevent',
        defVal: 0
    },
    {
        cmd: 'jsongetmacroevent',
        query: '/network.cgi?jsongetmacroevent'
    },
    {
        cmd: 'reboot',
        query: '/network.cgi?reboot'
    }
];

module.exports.sensorUnit = [
    {
        sensorUnit: 1,
        scale: 0.1,
        unitSymbol: '&#8451;'
    },
    {
        sensorUnit: 2,
        scale: 0.1,
        unitSymbol: '&#8457;'
    },
    {
        sensorUnit: 3,
        scale: 1,
        unitSymbol: '%'
    },
    {
        sensorUnit: 4,
        scale: 1,
        unitSymbol: 'lux'
    },
    {
        sensorUnit: 5,
        scale: 0.1,
        unitSymbol: 'Watt;'
    },
    {
        sensorUnit: 6,
        scale: 0.01,
        unitSymbol: 'Volt'
    },
    {
        sensorUnit: 7,
        scale: 0.001,
        unitSymbol: 'Amp'
    },
    {
        sensorUnit: 8,
        scale: 0.01,
        unitSymbol: '%'
    },
    {
        sensorUnit: 9,
        scale: 0.01,
        unitSymbol: 'kWh'
    },
    {
        sensorUnit: 12,
        scale: 1,
        unitSymbol: 'UVI'
    }
];

module.exports.ctrlCmd = [
    {
        "cmd": "abort",
        "uid": ""
    },
    {
        "cmd": "exclude",
        "uid": ""
    },
    {
        "cmd": "getdevice",
        "uid": "256"
    },
    {
        "cmd": "getconfig",
        "uid": "",
        "datas": [
            {
                "key": ""
            }
        ]
    },
    {
        "cmd": "getinterface",
        "uid": 0
    },
    {
        "cmd": "include",
        "uid": ""
    },
    {
        "cmd": "resethw",
        "uid": ""
    },
    {
        "cmd": "savedata",
        "uid": ""
    },
    {
        "cmd": "setconfig",
        "uid": "",
        "datas": [
            { "key": "", "val": "", "size": "" }
        ]
    },
    {
        "cmd": "switch",
        "uid": "",
        "chid": "",
        "val": "" //val: The control value. 0 → Switch OFF, 1 ~ 100 → Dimmer Level, 255 → Switch ON,
    },
    {
        "cmd": "sysbatstate"
    },
    {
        "cmd": "sysbatval"
    },
    {
        "cmd": "sysbtnstate"
    },
    {
        "cmd": "sysledaround",
        "val": ""
    },
    /*
        cmd: "sysledaround"
        val: The control value (using bit).
        Bit0: OFF, set the LED turn OFF.
        Bit1: ON, set the LED turn ON.
        Bit2: Flash, set the LED quick flash.
        Bit3: Slow flash, set the LED slow flash, about 1Hz.
        Bit4: Slow dimming, set the LED dimming, slow change the brightness.
        Notice: Only one bit can be set. If setting more than one bit, the lower bit has high priority.
    */
    {
        "cmd": "sysledlogo",
        "val": ""
    },
    {
        "cmd": "sysrtctime"
    },
    {
        "cmd": "syssound",
        "val": ""
    },
    /* val: The sound type.
        0: Turn off the siren immediate.
        1: Fire alarm
        2: Ambulance alarm
        3: Police Alarm
        4: Alarm
        5: Ding-Dong
        6: Beep
        */
    {
        "cmd": "syswatchdog",
        "val": ""
    },
    {
        "cmd": "thermostatfan",
        "uid": "",
        "val": ""
    },
    /* val: the value decides the fan mode.
        0: OFF
        1: means Auto/Auto Low
        2: means Low
        3: means Auto High
        4: means High */
    {
        "cmd": "thermostatmode",
        "uid": "",
        "val": ""
    },
    /* val: the value decides the mode or type.
        0: means OFF
        1: means Heating
        2: means Cooling
        3: means Auto */
    {
        "cmd": "thermostattemp",
        "uid": "",
        "temp": "",
        "unit": ""
    },
    {
        "cmd": "setassociate",
        "uid": "",
        "associateuid": "",
        "associategroup": ""
    },
    {
        "cmd": "removeassociate",
        "uid": "",
        "associategroup": "",
        "assciateuid": ""
    },
    {
        "cmd": "getassociate",
        "uid": "",
        "associategroup": ""
    },
    {
        "cmd": "getgroupnumber",
        "uid": ""
    },
    {
        "cmd": "mutichsetassociate",
        "uid": "",
        "associategroup": "",
        "assciateuid": "",
        "chid": "" //chid: channel ID of target device.
    },
    {
        "cmd": "mutichgetassociate",
        "uid": "",
        "associategroup": ""
    },
    {
        "cmd": "mutichremoveassociate",
        "uid": "",
        "associategroup": "",
        "assciateuid": "",
        "chid": ""
    },
    {
        "cmd": "simpleAVcontrol",
        "uid": "",
        "val": "" //val: the control value (ZXT-310 User manual)
    },
    {
        //To send command to prevent PIR or Door/Window sensor trigger Home Security or prevent remote sensor to control Home Security enabled or not.
        "cmd": "setmainscene",
        "uid": "",
        "val": "",
        "chid": "" //chid: The device channel ID 
    },
    {
        "cmd": "removefailnode",
        "uid": ""
    },
    {
        "cmd": "usercodeset",
        "uid": "",
        "userID": "",
        "codenum": "",
        "code1": "",
        "code2": "",
        "code3": "",
        "code4": ""
    },
    {
        "cmd": "usercodeget",
        "uid": "",
        "userID": ""
    },
    {
        "cmd": "usercoderemove",
        "uid": "",
        "userID": ""
    },
    {
        "cmd": "getswitch",
        "uid": ""
    },
    {
        "cmd": "ls",
        "uid": "",
        "val": "",
        "chid": ""
    },
    {
        "cmd": "setlowbattnotify",
        "uid": "",
        "val": "",
        "chid": ""
    },
    {
        "cmd": "sunrise_trig_time_get",
        "uid": "",
        "chid": "" //chid : which group setting , maximum is 8 groups
    },
    {
        "cmd": "sunrise_trig_time_set",
        "uid": "",
        "chid": "",
        "val": "" //val: time (unit is second) , allow +7200 ~ -7200 seconds
    },
    {
        "cmd": "sunset_trig_time_get",
        "uid": "",
        "chid": "" //chid : which group setting , maximum is 8 groups
    },
    {
        "cmd": "sunset_trig_time_set",
        "uid": "",
        "chid": "",
        "val": "" //val: time (unit is second) , allow +7200 ~ -7200 seconds
    },
    {
        "cmd": "switch_color_set",
        "uid": "",
        "val": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] //val: color array. 
        //ex "val":[5,0,0,1,0,2,128,3,128,4,192]
    },
    {
        "cmd": "all_switch_on",
        "uid": ""
    },
    {
        "cmd": "all_switch_off",
        "uid": ""
    },
    {
        "cmd": "sunrise_time_get",
        "uid": ""
    },
    {
        "cmd": "switch_all_set",
        "uid": "",
        "val": "",
        "chid": ""
    }
];
/*
10.2. Response Code Code Message
100=OK
101=Unknown Command
102=Invalid UID
103=Timeout
104=RF Busy
105=Internal Error
/*

/*
10.1.1.
abort
Abort the executing command.
10.1.2.
exclude
Remove a device from network.
10.1.3.
getdevice
Get the device information.
10.1.4.
getconfig
Get the configuration of the device.
10.1.5.
getinterface
Get the interface information.
10.1.6.
include
Include a device into network.
10.1.7.
resethw
Reset RF hardware.
10.1.8.
savedata
Save the data to file.
10.1.9.
setconfig
Set the configuration of the device.
10.1.10.
switch
Control a device basic ON/OFF or set value.
10.1.11.
sysbatstate
Get the battery state.
10.1.12.
sysbatval
Get the battery ADC value.
10.1.13.
sysbtnstate
The button state of the device.
10.1.14.
sysledaround
Control the around LED of the gateway.
10.1.15.
sysledlogo
Control the logo LED of the gateway.
10.1.16.
sysrtctime
Access the RTC time.
10.1.17
syssound
To play the alarm sound of the device
10.1.18
syswatchdog
To enable or disable the Watch Dog function
10.1.19
thermostatfan
To control thermostat fan
10.1.20
thermostatmode
To control thermostat mode

10.1.21
thermostattemp
To control thermostat temperature
10.1.22
setassociate
To set association between devices
10.1.23
removeassociate
To remove association between devices
10.1.24
getassociate
To get association status of device
10.1.25
getgroupnumber
To get max number of device supported groups
10.1.26
mutichsetassociate
Use mutichannel command to set associate with channel of device
10.1.27
mutichgetassociate
To get multichannel association status of device
10.1.28
mutichremoveassociate
To remove multichannel association between devices.
10.1.29
simpleAVcontrol
To send command to Remote Control(ZXT-310).
10.1.30
setmainscene
To prevent PIR or Door/Window sensor trigger Home Security or prevent remote sensor to control Home Security enabled or not.
10.1.31
removefailnode
To remove device without normal way(exclude) , this way is used when device is not response anymore and can't exclude.
10.1.32
usercodeset
To set door lock user code (maximum 6 number)
10.1.33
usercodeget
To get user code of door lock
10.1.34
usercoderemove
To remove user code of door lock
10.1.35
getswitch
To get device status
10.1.36
settampernotify
Enable send notification if trigger tamper
10.1.37
setlowbattnotify
Enable send notification if device in low battery status
10.1.38
sunrise_trig_time_get
Get sunrise trigger setting
10.1.39
sunrise_trig_time_set
Set sunrise trigger setting
10.1.40
sunset_trig_time_get
Get sunset trigger setting
10.1.41
sunset_trig_time_set
Set sunset trigger setting
10.1.42
switch_color_set
Set color of color dimmer
10.1.43
all_switch_on
Turn on all switch
10.1.44
all_switch_on
Turn off all switch
10.1.45
sunrise_time_get
Get sunrise time
10.1.46
sunrise_time_set
Set sunrise time
10.1.47
SWITCH_ALL_SET
SWITCH ALL SET

*/

