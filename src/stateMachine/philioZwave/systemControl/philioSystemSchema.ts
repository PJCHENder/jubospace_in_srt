

module.exports = {
    initState: 'philioInit',
    philioInit: {
        actions: { onEnter() {}, onExit() {}, },
        transitions: {
            deviceInit: {
                target: 'philioInit',
                action() {}
            },             
            deviceInitDone: {
                target: 'philioSequence',
                action() {
                    console.log('philioInit ==> philioSequence')
                }
            },
            deviceError: {
                target: 'philioReboot',
                action() {
                    console.log('philioInit ==> philioReboot')
                }
            }                  
        }
    },
    philioSequence: {
        actions: { onEnter() {}, onExit() {}, },
        transitions: {
            waitSequence: {
                target: 'philioSequence',
                action() {}
            },
            acquiredSequence : {
                target: 'philioNormal',
                action() {
                    console.log('philioSequence ==> philioNormal')
                }
            }
        }
    },
    philioNormal: {
        actions: { onEnter() {}, onExit() {}, },
        transitions: {
            deviceNormal: {
                target: 'philioNormal',
                action() {}
            },
            philioSequence: {
                target: 'philioSequence',
                action() {
                    console.log('philioNormal ==> philioSequence')
                }
            },            
            deviceInit: {
                target: 'philioInit',
                action() {}
            },             
            deviceError: {
                target: 'philioReboot',
                action() {
                    console.log('philioNormal ==> philioReboot')
                }
            }                                                      
        }
    },
    philioReboot: {
        actions: { onEnter() {}, onExit() {}, },
        transitions: {
            philioRebootStart: {
                target: 'philioReboot',
                action() {}
            },
            philioRebootRetry: {
                target: 'philioReboot',
                action() {}
            },            
            philioRebootComplete: {
                target: 'philioInit',
                action() {}
            }                                                     
        }
    }        
}