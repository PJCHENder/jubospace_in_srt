'use strict'

const fs = require('fs');
const path = require('path');
const _ = require('lodash');

const sleep = require('util').promisify(setTimeout)

//const JuboEvent = require('./../../commonEvent/commonEvent').commonEmitter
const Netlist = require('./../../../netlist/netlist');
const rp = require('request-promise');
const philioApiHandler = require('./../philioApi/philioApiHandler')
const philioApi = require('./../philioApi/philioDeviceApi');

const TIMEOUT_200MS = 200
const TIMEOUT_400MS = 400
const TIMEOUT_600MS = 600
const TIMEOUT_800MS = 800

import { NetworkDeviceObject,
    PhilioDeviceListQuery,
    PhilioChannelListQuery,
    PhilioConnectOption,
    PhilioSensorLog,
    PhilioGatewayObject } from "./../interfaces/philioInterface";

const wait = (timeout: number) => {
    return new Promise((resolve,reject) => {
        setTimeout( ()=> {
            resolve()
        },timeout)
    })
} 

const findPhilioGateway = () => {
    return new Promise ((resolve,reject) => {
        try{
            let gatewayIfName = Netlist.find("Philio");
            resolve(gatewayIfName)
        } catch(error){
            reject(error)
        }
    })
}

const initNetSetting = async (philioGateway:PhilioGatewayObject) => {
    try{        
        console.log('initNetSetting Working........')
        let connMode = await philioApiHandler.networkConfig(philioGateway, "getconnectmode", "");
        console.log("Philio  ==>  " + connMode);
        let that = await philioApiHandler.networkConfig(philioGateway, "setconnectmode", "DHCP");
        let gatewayIp = await philioApiHandler.networkConfig(philioGateway, "getgateway", "");
        console.log("Philio  ==>  " + gatewayIp);
        let dnsServer = await philioApiHandler.networkConfig(philioGateway, "getdnsserver", "");
        console.log("Philio  ==>  " + dnsServer);
        let netMask = await philioApiHandler.networkConfig(philioGateway, "getnetmask", "");
        console.log("Philio  ==>  " + netMask);
        let philioIp = await philioApiHandler.networkConfig(philioGateway, "getip", "");
        console.log("Philio  ==>  " + philioIp);

    } catch (error) {
        console.log("fn: initNetSetting: " + error);
    }
}
    
const createPhilioGateway = (deviceObject: NetworkDeviceObject,
        connectOption:PhilioConnectOption,
        philioJsonResp: string) => {
    return new Promise((resolve, reject) => {
        try{
            let temp = JSON.parse(philioJsonResp)

            let gatewayObj = {...temp.iface[0],
                        ...temp.hw,
                        ...connectOption,
                        ...deviceObject}
            //console.log(gatewayObj)
            resolve(gatewayObj)
        } catch (error) {
            reject(error)
        }
    })
}    

const getDeviceList = (philioJsonResp: string) => {
    return new Promise((resolve, reject) => {
        try{
            let tempObj = {
                "group":"",
                "location":"",
                "description":"",
                "powerLevel":100,
                "powerStatus":"",
                "tamperStatus":"noTampered",
                "tamperTimeStamp":0,
                "userDefineName":""
    
            }
            let philioDevices = [];
            
            if(JSON.parse(philioJsonResp).control.respcode == 100){
                let fullDeviceList = JSON.parse(philioJsonResp).device;
                for(let {   "uid": uid,
                            "Product ID": productId,
                            "home_id": homeId,
                            "channel": channel } of fullDeviceList) {
                        let deviceChannel = [];

                        for(let {chid,functype,name} of channel){
                            let channelList: any = {
                                chid,
                                functype,
                                name
                            }

                            //Disabling
                            if(channelList.functype != '44' &&      //Scene
                                channelList.functype != '0' &&      //UNKNOWN Scene           
                                channelList.functype != '27' &&
                                channelList.uid != 510){      //Remote

                                channelList.functype = philioApi.funcType.find(obj => obj.funcCode == channelList.functype).funcName;
                                deviceChannel.push(channelList)
                            }
                        }

                        let processDeviceList : any = {
                            uid, productId,homeId,deviceChannel
                        }
                        let tempId = processDeviceList.productId
                        processDeviceList.productId = philioApi.productInfo.find(obj => obj.productId == tempId).part;
                        tempObj.description = philioApi.productInfo.find(obj => obj.productId == tempId).product;
                        philioDevices.push({...processDeviceList, ...tempObj})
                }
                //console.log(philioDevices)

                resolve(philioDevices)
            }
            
        } catch (error) {
            console.log("fn: getPhilioDeviceList: "+error);
            if(error.name==="RequestError"){
                //JuboEvent.emit('philioRequestError')
            }
        }
    })
}

const createChannelList = (philioJsonResp: string) => {
    return new Promise((resolve, reject) => { 
        // console.log(JSON.parse(getDeviceResp));
        let sceneSwitchList = [
            {
                "sceneName":"enteringLight",
                "sceneVal":null
            },
            {
                "sceneName":"exitingLight",
                "sceneVal":null
            },            
            {
                "sceneName":"readingLight",
                "sceneVal":null
            },
            {
                "sceneName":"restingLight",
                "sceneVal":null
            },            
            {
                "sceneName":"morningLight",
                "sceneVal":null
            },
            {
                "sceneName":"sleepingLight",
                "sceneVal":null
            },
            {
                "sceneName":"allOnLight",
                "sceneVal":255
            },
            {
                "sceneName":"allOffLight",
                "sceneVal":0
            },
            {
                "sceneName":"emergencyLight",
                "sceneVal":null                
            }                                               
        ]
        let sceneDimmerList = [
            {
                "sceneName":"enteringLight",
                "sceneVal":null
            },
            {
                "sceneName":"exitingLight",
                "sceneVal":null
            },            
            {
                "sceneName":"readingLight",
                "sceneVal":null
            },
            {
                "sceneName":"restingLight",
                "sceneVal":null
            },            
            {
                "sceneName":"morningLight",
                "sceneVal":null
            },
            {
                "sceneName":"sleepingLight",
                "sceneVal":null
            },
            {
                "sceneName":"allOnLight",
                "sceneVal":100
            },
            {
                "sceneName":"allOffLight",
                "sceneVal":0
            },
            {
                "sceneName":"emergencyLight",
                "sceneVal":null                
            }                                              
        ]
        let customObject = {
            "group":"",
            "location":"",
            "description":"",
            "userDefineName":""
        }

        let sceneSwitchObject = {
            "sceneValMax":null,
            "sceneValMin":null,
            "sceneSetting":sceneSwitchList
        }
        let sceneDimmerObject = {
            "sceneValMax":null,
            "sceneValMin":null,
            "sceneSetting":sceneDimmerList
        }
        let philioChannels = [];
        try {
            if(JSON.parse(philioJsonResp).control.respcode == 100){
                let fullDeviceList = JSON.parse(philioJsonResp).device;   
                // console.log(fullDeviceList); 
                for (let {
                    "uid": uid,
                    "Product ID": productId,
                    "home_id": homeId,
                    "channel": list } of fullDeviceList) {
                    for (let { chid, functype, name } of list) {
                        let processList:any = { uid, 
                                                homeId,
                                                productId, 
                                                functype, 
                                                chid, 
                                                name
                                            }
                        //Disable
                        
                        if(processList.functype != 44 &&      //Scene
                            processList.functype != 0 &&      //UNKNOWN Scene           
                            processList.functype != 27 &&
                            processList.uid != 510 &&           //Remote Combo all
                            ((processList.functype == 22 ||       //switch
                                processList.functype == 23 ||       //meter switch
                                processList.functype == 24) &&   //Dimmer 
                                processList.chid != 3)           //Get rid of switch chid 3
                            ){             
                            //console.log(processList)
                            processList.functype = philioApi.funcType.find(obj => obj.funcCode == processList.functype).funcName;
                            let tempId = processList.productId
                            processList.productId = philioApi.productInfo.find(obj => obj.productId == tempId).part;
                            customObject.description = philioApi.productInfo.find(obj => obj.productId == tempId).product;
                            //console.log(processList)
                            if(processList.functype === "Switch" ||
                                processList.functype === "Meter Switch"){
                                    sceneSwitchObject.sceneValMax=255;
                                    sceneSwitchObject.sceneValMin=0;
                                    sceneSwitchObject.sceneSetting.find(x=>x.sceneName === 'allOnLight').sceneVal = 255;
                                    sceneSwitchObject.sceneSetting.find(x=>x.sceneName === 'allOffLight').sceneVal = 0;
                                    philioChannels.push({...processList, ...customObject, ...sceneSwitchObject});
                                    //console.log(sceneObject)
                            } else if( processList.functype === "Dimmer"){
                                    sceneDimmerObject.sceneValMax=100;
                                    sceneDimmerObject.sceneValMin=0;
                                    sceneDimmerObject.sceneSetting.find(x=>x.sceneName === 'allOnLight').sceneVal = 100;
                                    sceneDimmerObject.sceneSetting.find(x=>x.sceneName === 'allOffLight').sceneVal = 0;
                                   
                                    let test2 = Object.assign(processList,customObject,sceneDimmerObject)
                                    //console.log(test2);
                                    philioChannels.push(test2);
                                       
                            } else {
                                philioChannels.push({...processList, ...customObject});                                
                            }
                            
                        //API: https://docs.google.com/document/d/150DyQOyCVV7KtQlPwyHJk2Pf4JNkuNxS/edit
                        }
                    }
                }                            
            } else {
                //throw error
            }        
            // console.log(JSON.stringify(philioDevices))
            resolve(philioChannels);
        } catch (error) {
            console.log("fn: createPhilioChannelList: "+error);
            if(error.name==="RequestError"){
                //JuboEvent.emit('philioRequestError')
            }
        }
    });
}   //createPhilioChannelList

const writeJsonToFile = (file,data) => {
    return new Promise ((resolve,reject) => {
        try{
            fs.writeFile(file,data,() => {
                
                resolve("update successful")
            })             
        } catch (error) {
            console.log("fn: writeJsonToFile: " + error);  
            reject(error)
        }

    });
}   //end writeJsonToFile

const updateChannelListToFile = (filePath: string, philioJsonResp: string) => {
    return new Promise((resolve, reject) => {
        try{
            let philioChannelListJson = JSON.parse(philioJsonResp)
            if(philioChannelListJson != 'undefined' || philioChannelListJson != 'null'){
                let test = writeJsonToFile('./public/philioChannelListRaw.json',philioJsonResp)
                //console.log(test)
                resolve(philioChannelListJson)     
            }      
        } catch(error){
            reject(error)
        }
    })
}


const updateDeviceFromFile = (deviceListFromGateway) => {
    return new Promise ((resolve,reject) => {
        try{
            let fsCheck = fs.readFileSync(path.join(process.cwd() + '/../dist/public','philioDeviceListRaw.json'));
            if(fsCheck.byteLength==0){
                writeJsonToFile('./public/philioDeviceListRaw.json',JSON.stringify(deviceListFromGateway))

            } else {
                let philioDeviceFromFile : PhilioDeviceListQuery = JSON.parse(fs.readFileSync(path.join(process.cwd() + '/../dist/public','philioDeviceListRaw.json')));
                // console.log(devicelist)    
                // console.log(philioDeviceFromFile)
                for(let philioDevice of philioDeviceFromFile){
                    if(philioDevice.group != '' || philioDevice.location != ''){
                        // console.log(philioDevice)
                        for(let device of deviceListFromGateway){
                            let result = _.isEqual(_.omit(device,['group','location']),
                                                _.omit(philioDevice,['group','location']))
                            if(result == true){
                                device.group = philioDevice.group;
                                device.location = philioDevice.location;
                                // console.log(device)
                            }
                        }             
                    }
                }
                writeJsonToFile('./public/philioDeviceListRaw.json',JSON.stringify(deviceListFromGateway))                
            }       
            resolve(deviceListFromGateway)
        } catch(error){
            reject(error)
        }
    })
}

const updateChannelFromFile = (channelListFromGateway : PhilioChannelListQuery) => {
    return new Promise((resolve, reject) => {
        try{
            //let fsCheck = fs.readFileSync(path.join(process.cwd() + '/../dist/public','philioChannelListRaw.json'));
            let fsCheck = fs.readFileSync('./public/philioChannelListRaw.json')
            //console.log(fsCheck)
            if(Buffer.byteLength(fsCheck)==0){
                console.log('new device list')
                writeJsonToFile('./public/philioChannelListRaw.json',JSON.stringify(channelListFromGateway))
            } else {
                let philioChannelFromFile : PhilioChannelListQuery = JSON.parse(fs.readFileSync(path.join(process.cwd() + '/../dist/public','philioChannelListRaw.json')));
                
                //console.log(philioChannelFromFile)
                for(let philioDevice of philioChannelFromFile){
                         
                    for(let device of channelListFromGateway){
                        
                        let result = _.isEqual(_.omit(device,['group','location','userDefineName','sceneValMax','sceneValMin','sceneSetting']),
                                            _.omit(philioDevice,['group','location','userDefineName','sceneValMax','sceneValMin','sceneSetting']))
                        
                        if(result == true){
                            //console.log(device)
                            //console.log(philioDevice)
                            device.group = philioDevice.group;
                            device.location = philioDevice.location;
                            device.userDefineName = philioDevice.userDefineName;
                            if(device.functype === 'Switch' ||
                                device.functype === 'Meter Switch' ||
                                device.functype === 'Dimmer'){
                                    device.sceneValMax = philioDevice.sceneValMax;
                                    device.sceneValMin = philioDevice.sceneValMin;
                                    device.sceneSetting = philioDevice.sceneSetting;
                            }
                            //console.log(device)
                        }           
                    }             
                }
                writeJsonToFile('./public/philioChannelListRaw.json',JSON.stringify(channelListFromGateway))
            }
            resolve(channelListFromGateway)

        } catch (error) {
            reject(error);
        }
    })
}

const eventLogToSensorLog = (eventLogResp: any) => {
    return new Promise((resolve, reject) => { 
        let eventLogDump = eventLogResp;
        let jsonTemp;
        try{
            //console.log(eventLogDump);
            let sensorLogArray = [];
            if (eventLogDump === "" || eventLogDump === undefined){
                throw new Error('philio null response')
            } else if (eventLogDump.startsWith("jsongetevent")) {
                throw new Error('philio-jsongetevent-fail')

            } else {
                let eventLogTemp = eventLogResp.split('<br>')[0];
                jsonTemp = JSON.parse(eventLogTemp).eventLog;
                
                for(let eventLog of jsonTemp){
                    //console.log(eventLog);
                    let {
                        "basicValue": basicValue,
                        "eventCode": eventCode,
                        "battery": battery,
                        "dataUnit":dataUnit,
                        "sensorValue":sensorValue,
                        "timeStamp":timeStamp,
                        "funcType": funcType,
                        "uid": uid ,
                        "funcName": funcName,
                        "sequence": sequence
                    } = eventLog;
    
                    let sensorTemp : PhilioSensorLog = {
                        basicValue,
                        eventCode,
                        battery,
                        dataUnit,
                        sensorValue,
                        timeStamp,
                        funcType,
                        uid,
                        funcName,
                        sequence };
                    let funcTypeStr = sensorTemp.funcType.toString();
                    //console.log('before')
                    //console.log(funcType)
                    sensorTemp.funcType = philioApi.funcType.find((x: { funcCode: any; }) => x.funcCode === funcTypeStr).funcName;
                    //console.log('after')
                    //console.log(funcType)

                    //console.log(sensorTemp);
                    for (let k of Object.keys(sensorTemp)) {
                        sensorTemp[k] = '' + sensorTemp[k];
                    };
                    
                    sensorLogArray.push(sensorTemp);                    
                }
                // console.log(sensorLogArray)              
                resolve(sensorLogArray);              
            }
        } catch(error){
            console.log("fn: eventLogToSensorLog : " + error);
            reject(error);
        }
    });
}   //end eventLogToSensorLog

const eventLogHandler = async (sequence:number,gateway) => {
    try{
        let now = new Date().toISOString()
        console.log(`[seq]: ${sequence}: ${now}`)
        let eventLogDump = await philioApiHandler.networkConfig(gateway, "jsongetevent", sequence);
        let sensorLogArray = await eventLogToSensorLog(eventLogDump);

        return sensorLogArray;   
    } catch (error){
        console.log(`fn: eventLogHandler ${error}`)
        return error
    }
}

const readUserDefineName = async (deviceObject: PhilioGatewayObject) => {
    try{
        let jsonResp = await philioApiHandler.readDeviceTargetName(deviceObject)
        //let userDefineName = await updateUserDefineNameToFile(jsonResp)

        //return userDefineName

    } catch (error) {

    }
}

const doLightSceneAuto = async (deviceObject: PhilioGatewayObject, sceneName:string) => {
    const MAX_RETRIES = 10;
    for(let i=0; i<=MAX_RETRIES; i++){
        try{
            let sceneResp = await Philio.doLightScene(deviceObject,sceneName)
            if(sceneResp.split('<br>')[0]===sceneName && sceneResp.split('<br>')[1] === 'doscene done'){
                
                i=MAX_RETRIES;
                let stringResp = 'lightmode-'+sceneName
                //commonEvent.emit('done-light-scene',stringResp)
            }
        } catch (error){
            const timeout = i*1000;
            console.log('Waiting', timeout, 'msec');
            await wait(timeout)
            console.log("doLightSceneAuto Retrying", error.message, i);
        }
    }
}

const findCurrentSequence = (eventLogResp: any) => {
    return new Promise((resolve, reject) => {
        let tempDump;
        try{
            tempDump=eventLogResp
            if(eventLogResp=== null || eventLogResp === undefined){
                throw new Error('eventLogResp is null')
            } else {
                let eventLogTemp = eventLogResp.split('<br>')[0];
                let eventlog = JSON.parse(eventLogTemp).eventLog;
                let currentEpoch: number = Math.floor(Date.now()/1000);
                if(eventlog.length != 0){
                    let firstSequence = eventlog[0].sequence;

                    if(Number.isInteger(firstSequence)){
                        for (let i = 0; i < eventlog.length; i++) {
            
                            let readTimeStamp = eventlog[i].timeStamp;
                            //console.log(readTimeStamp)    
                            // Read Log within 2 minutes
                            if ((currentEpoch - readTimeStamp) < 120) {
                                let twoMinute = i+firstSequence
                                // console.log(`< Two Minute Seq: ${twoMinute}`)
                                resolve(twoMinute);
                            }
                        }
                        //console.log(`length: ${eventlog.length}`)
                        let end = eventlog[eventlog.length-1].sequence
                        console.log(`Last Seq: ${end}`)
                        resolve(end) 
                    } else {
                        resolve(1);
                    }
                } else {
                    console.log('eventlog is empty []')
                    resolve(1)
                }
            }
        } catch(error){
            console.log("fn: findCurrentSequence " + error);
            console.log(tempDump)
            //JuboEvent.emit('retry-sequence');
        }
    })
}

const readDeviceTargetName = (deviceObject: PhilioGatewayObject) => {
    return new Promise((resolve, reject) => {
        try{
            let deviceTargetName = {
                uid: '',
                chid: '',
                targetName: ''
            }

            if (deviceObject) {
                let connectOptions = deviceObject.connectOption
                
                connectOptions.uri = 'http://' + deviceObject.deviceIp + '/network.cgi?jsongetall'
                //console.log(connectOptions.uri)
                let respStr = rp(connectOptions)
                                    .then()
                                    .catch((error) =>{
                                        console.log(`fn:readPhilioDeviceTargetName  targetName ${error}`)
                                        //JuboEvent.emit('philioRequestError')
                                    })
                
                let respJson = JSON.parse(respStr.split('<br>')[0])
                if(respJson){
                    console.log(respJson.targetNames)


                    resolve(respJson.targetNames)
                }
            }
        } catch (error) {
            console.log(`fn: readTargetName ${error}`)
            reject(error)
        }
    })
}

const writeDeviceTargetName = (deviceObject: PhilioGatewayObject) => {
    return new Promise((resolve, reject) => {
        try{
            if (deviceObject != null || deviceObject != undefined) {
                let connectOptions = deviceObject.connectOption
                
                connectOptions.uri = 'http://' + deviceObject.deviceIp + '/network.cgi?jsonsetall='
                //console.log(connectOptions.uri)
                let sceneConnect = rp(connectOptions)
                                    .then()
                                    .catch((error) =>{
                                        console.log(`fn:writePhilioDeviceTargetName  sceneConnect ${error}`)
                                        //JuboEvent.emit('philioRequestError')
                                    })
            
                resolve(sceneConnect);
            } else {

            }
        } catch (error){
            console.log(`fn: writeUserDefineName ${error}`)
            reject(error)
        }
    })
}

const isJson = (str) => {
    try{
        JSON.parse(str)
    } catch(e){
        return false
    }
    return true
}

const procJsonGetAll = async (deviceObject: PhilioGatewayObject) => {
    try {
            let queryResult = await philioApiHandler.jsonGetAll(deviceObject)
            let queryStr = queryResult.split('<br>')[0]
            if(isJson(queryStr)){
                let jsonGetAllObj = JSON.parse(queryStr)
                return jsonGetAllObj
            }
            
    } catch(error) {
        console.log(`fn: procJsonGetAll ${error}`)
        
    }

}

exports.findPhilioGateway = findPhilioGateway;
exports.initNetSetting = initNetSetting;

exports.eventLogHandler = eventLogHandler;
exports.eventLogToSensorLog = eventLogToSensorLog;
exports.createPhilioGateway = createPhilioGateway;
exports.getDeviceList = getDeviceList;

exports.createChannelList = createChannelList;
exports.updateChannelListToFile = updateChannelListToFile;

exports.updateChannelFromFile = updateChannelFromFile;
exports.updateDeviceFromFile = updateDeviceFromFile;

exports.writeDeviceTargetName = writeDeviceTargetName
exports.readDeviceTargetName = readDeviceTargetName

exports.doLightSceneAuto = doLightSceneAuto;

exports.findCurrentSequence = findCurrentSequence;

exports.procJsonGetAll = procJsonGetAll

