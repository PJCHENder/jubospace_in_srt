'use strict'
export {}
const fsm = require('./../../stateMachine/statemachine')
const panicButtonController = require('./../emergencyAlert/panicButtonFsm')
const iPadSysGrpcHandler = require('./deviceSystemGrpcAdapter')
const iPadPanicGrpcHandler = require('./../emergencyAlert/genericPanicButtonAdapter')
const appEventHanlder = require('./../../commonEvent/commonEvent').commonEmitter
const EventEmitter = require('events')

import { iPadControllerInterface,
        iPadDeviceDbInterface,
        genericSystemInfo,
        WsConnectToken } from './interfaces/myHealthAppInterface'

const IPAD_BATTERY_LOW_LEVEL = 25

class iPadClientController extends EventEmitter {
    
    initController: any
    systemInfo: genericSystemInfo
    wsHandler: any
    eventHandle: any
    constructor (connectToken:WsConnectToken, ws: any) {
        super()
        this.initController = this.initIpadAssociate()
        this.systemInfo = {
            "batteryLevel": ''
        }
        this.wsHandler = ws
        this.eventHandle = appEventHanlder

    }

    private initIpadAssociate = () => {
        console.log('initIpadAssociate')
    }

    public handleIncomingMessage = async (iPad: iPadDeviceDbInterface, msg:string):Promise<any> => {
        try{
            //console.log(msg)
            let grpcResp:any
            let head = msg.split("=")[0]
            let body = msg.split("=")[1]
            let deviceId = head.split('#')[0].split('/')[1]
            let cmd = head.split('#')[1]
            if(deviceId === iPad.boxDeviceId){
                switch(cmd) {
                    case 'panicButton':
                        grpcResp =  await this.handlePanicButtonRequest(iPad,body)
                        if(grpcResp){
                            console.log(grpcResp)
                            if(grpcResp.serverResp === 'resp DeviceEvent from server'){
                                return {grpcResp,cmd:cmd,body:body}
                            }
                        }
                    break;

                    case 'lightControl':
                        grpcResp = await this.handleLightControlRequest(iPad,body)
                        if(grpcResp){
                            console.log(grpcResp)
                            if(grpcResp.serverResp === 'resp DeviceEvent from server'){
                                return grpcResp
                            }
                        }                        
                    break;

                    case 'systemInfo':
                        grpcResp = await this.handleSystemInfo(iPad,body)
                        if(grpcResp){
                            console.log(grpcResp)
                            if(grpcResp.serverResp === 'resp DeviceState from server'){
                                return grpcResp
                            }
                        }                        

                    break;

                    default:

                    break;
                }
            } else {
                throw new Error('iPad deviceId un-matched')
            }

        } catch (error) {
            console.log(`fn: handleIncomingMessage ${error}`)
        }
    }

    public handleSystemInfo = (iPad: iPadDeviceDbInterface, cmdBody:string) => {
        return new Promise((resolve,reject) => {
            try {
                this.systemInfo.batteryLevel = JSON.parse(cmdBody).batteryLevel
                if(parseInt(this.systemInfo.batteryLevel) <= IPAD_BATTERY_LOW_LEVEL){
                    resolve(iPadSysGrpcHandler.batteryLowGrpcAdapter(iPad, this.systemInfo))
                } else {
                    console.log('Battery Level > 25%')
                }
                
            } catch(error) {
                console.log(`fn: handleSystemInfo ${error}`)
                reject(error)
            }
        })
    }
    public handlePanicButtonRequest = (iPad: iPadDeviceDbInterface, cmdBody:string) => {
        return new Promise((resolve,reject) => {
            try{
                
                switch(cmdBody) {
                    case 'cleared':
                        resolve(iPadPanicGrpcHandler.panicButtonClearedGrpcAdapter(iPad))
                        //console.log(test)
                    break;

                    case 'pressed':
                        resolve(iPadPanicGrpcHandler.panicButtonPressedGrpcAdapter(iPad))
                        //console.log(test1)
                    break;

                    default:

                    break;
                }
            } catch(error) {
                console.log(`fn: handlePanicButtonRequest ${error}`)
                reject(error)
            }
        })
    }

    public handleLightControlRequest = (iPad: iPadDeviceDbInterface, cmdBody:string) => {
        return new Promise ((resolve,reject) => {
            try{
                console.log('handleLightControlRequest')
                console.log(cmdBody)
                resolve(true)

            } catch(error){
                console.log(`fn:handleLightControlRequeset ${error}`)
                reject(error)
            }
        })
    }



    
}



module.exports = iPadClientController
