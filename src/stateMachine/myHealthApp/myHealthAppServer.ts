'use strict'

import { v4 as uuidv4 } from 'uuid';

const wss = require('ws')
const http = require('http')
const httpServer = http.createServer()
const iPadController = require('./iPadController')
const utils = require('./../../utils/utilities')

import type { GenericSpaceInfo } from './../../interfaces/genericInterface'
import { WsConnectToken, 
        iPadControllerInterface,
        iPadDeviceDbInterface,
        panicButtonStateInterface } from './interfaces/myHealthAppInterface'
const WS_CONN_TIMEOUT = 30000

interface myHealthWsServer {
    wsServerHandler 
}

const wsConnectOptions = {
    port: 8888,
    server: httpServer,
    clientTracking: true,
}

class MyHealthAppServer implements myHealthWsServer  {
    wsServerHandler: any
    iPadHandlers : Array <any>
    wsIpadClients: Array<WsConnectToken>
    timerHandler: any
    spaceInfo: GenericSpaceInfo
    philioGateway: any
    mwgGateway: any
    vayyarGateway: any
    //testCommon: any

    constructor(JuboSpace: any) {
        //console.log(JuboSpace)
        this.wsIpadClients = []
        this.spaceInfo = JuboSpace.spaceInfo
        this.philioGateway = JuboSpace.philio
        this.mwgGateway = JuboSpace.mwg
        this.vayyarGateway = JuboSpace.vayyar
        this.wsServerHandler = new wss.Server(wsConnectOptions)
        this.serverStart()
        //this.testCommon = iPadController.testCommonEmitter()
    }
    private noop = () => {}
    private interval = setInterval(function ping(){

    }, WS_CONN_TIMEOUT);

    private serverStart = () => {
        try{
            
            this.wsServerHandler.on('connection', (ws, req) => {
                
                let iPadHandler : iPadControllerInterface
                ws.isAlive = true;
                ws.id = uuidv4()
                ws["connectAuth"] = "unknown"
                let clientIp = req.socket.remoteAddress.match(/\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/)[0]
                let clientInfo = {
                    uid: ws.id,
                    deviceId: '',
                    patientId: '',
                    isAlive: true,
                    ipAddress: clientIp,
                    keepAliveTimeout: ''+WS_CONN_TIMEOUT+'ms'
                }
                
                ws.send('/connectToken='+JSON.stringify(clientInfo))

                ws.on('ping',this.wsHeartbeatPing)
                ws.on('pong',this.wsHeartbeatPong)
                ws.on('error',(error)=>{
                    console.log(`error ${error}`)
                })
                ws.on('close',()=>{
                    console.log(ws.id+' => closed->terminating')
                    // Update CouchDb Device Info , ie. lastConnectTime


                    this.wsIpadClients = this.removeClient(this.wsIpadClients,'deviceId',ws.id)
                    ws.isAlive = false
                    ws.terminate()                   
                    console.log(this.wsIpadClients)
                })
                ws.on('message',(msg) => {
                    try{
                        if(ws.connectAuth === undefined){
                            console.log('connecAuth undefined')
                        } else if (ws.connectAuth === 'unknown'){
                            console.log('connecAuth unknown')
                        } else if (ws.connectAuth === 'accepted'){
                            console.log('connecAuth accepted')
                        } else if (ws.connectAuth === 'rejected'){
                            console.log('connecAuth rejected')
                        }
                        //console.log(msg)
                        let head = msg.split("=")[0]
                        let body = msg.split("=")[1]
                        let iPadDevices = this.spaceInfo.deviceInfo.iPadDevices
    
                        let iPadDevice : iPadDeviceDbInterface                 
                        
                        if(head === '/connectResp'){
                            let connectTokenResp : WsConnectToken = JSON.parse(utils.removeByteOrderMark(body))
                            //console.log(connectTokenResp)
                            iPadDevice = iPadDevices.find((x: { boxDeviceId: string; }) => x.boxDeviceId === connectTokenResp.deviceId)    
                            //console.log(iPadDevice)
                            if(iPadDevice === undefined){
                                console.log('new ipad found, create new ipad in database')
                                //
                            } else {
                                //deviceId found in couchDb
                                ws.id = iPadDevice.boxDeviceId
                                ws.connectAuth = 'accepted'
                                ws.send('/connectAuth=accepted')
                                iPadHandler  = new iPadController(connectTokenResp, ws)
                                this.wsIpadClients.push({...connectTokenResp,...iPadHandler})
                            }
                        } else {
                            if(ws.connectAuth === 'accepted'){
                                let deviceId = head.split("#")[0].split('/')[1]
                                iPadDevice = iPadDevices.find((x: { boxDeviceId: string; }) => x.boxDeviceId === deviceId)  
                                iPadHandler.handleIncomingMessage(iPadDevice, msg).then((grpcResp)=>{
                                    if(grpcResp != undefined){
                                        switch(grpcResp.cmd){
                                            case 'panicButton':
                                                    ws.send('/'+ deviceId + '#'+ grpcResp.cmd + '='+grpcResp.body+'_success')
                                            break;
        
                                            case 'lightControl':
                                                console.log('in lightControl')
        
                                            break;
        
                                            default:
                                                console.log('in default')
                                                
                                            break;
                                        }
                                    }
    
                                })
                            } else if(ws.connectAuth === 'rejected'){

                            } else {

                            }


                        }
                    } catch (error) {
                        console.log(`fn: ws.on('message',(msg) ${error}`)
                    }
                })


            })
            this.wsServerHandler.on('close',(a,b,c,d)=>{
                console.log(a)
                console.log(b)
                console.log(c)
                console.log(d)
                clearInterval(this.interval);
            }) 
        } catch (error) {
            console.log(`fn: serverStart ${error}`)
        } finally {
            console.log('ws server initializing')
            this.timerHandler = setInterval(this.serverHeartbeat.bind(this),WS_CONN_TIMEOUT)
        } 
    }
    private serverHeartbeat = () => {
        try {
            // console.log(this.wsIpadClients)
            // console.log(this.wsServerHandler.clients.size)
            this.wsServerHandler.clients.forEach((ws) => {
                console.log('heartbeat')
                if (ws.isAlive === false) return ws.terminate();
             
                ws.isAlive = false;
                ws.ping(this.noop);
              });
        } catch (error) {
            console.log(`fn: serverHeartbeat ${error}`)
            throw new Error('wss heartbroken')
        }
    }
    private wsHeartbeatPing = () => {    
        for (let client of this.wsServerHandler.clients) {
            //console.log(`ping received ${client.id}`)
            client.isAlive = true;
        }
    } 
    private wsHeartbeatPong = () => {
        for (let client of this.wsServerHandler.clients) {
            //console.log(`pong received ${client.id}`)
            client.isAlive = true;
        }
    }
    private removeClient = (array:Array<WsConnectToken>,key:string,value:string):Array<WsConnectToken> => {
        const index = array.findIndex(obj => obj[key] === value);
        return index >= 0 ? [
            ...array.slice(0, index),
            ...array.slice(index + 1)
        ] : array;
    }
    
    public updateSpaceInfo = () => {
        try{
            //this.spaceInfo = 
            this.vayyarGateway.setSpace ()
            this.mwgGateway.setSpace ()
            this.philioGateway.setSpace ()

        } catch(error) {
            if(error.message === 'TypeError'){
                console.log(error)
            } else {
                console.log(`fn: updateSpaceInfo ${error}`)
            }
        }
    }

    public updateDeviceList = () => {
        try {

        } catch(error) {
            console.log(`fn: updateDeviceList`)
        }
    }
}

module.exports = MyHealthAppServer