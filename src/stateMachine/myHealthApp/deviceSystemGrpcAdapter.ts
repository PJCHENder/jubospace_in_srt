'use strict';

const deviceSystemHandler = require('./../../grpc/juboSpaceDeviceClient').grpcDeviceClientHandler
//const utils = require('./../../utils/utilities')
import { iPadDeviceDbInterface,
        genericSystemInfo } from './interfaces/myHealthAppInterface'

const batteryLowGrpcAdapter = async (deviceInterface: iPadDeviceDbInterface | any,
                                    systemInfo: genericSystemInfo) => {
    try {
        //console.log(deviceInterface)
        //console.log(systemInfo)
        return await deviceSystemHandler.DeviceStateGrpcUpdate({
            "NISPatientId": deviceInterface.patientId,
            "boxDeviceId": deviceInterface.boxDeviceId,
            "name": deviceInterface.deviceName,
            "location": deviceInterface.location.facility + ' - ' + deviceInterface.location.location,
            "room": deviceInterface.location.roomNum,
            "state": 'Battery Low',
            "description": 'Current Battery Level: ' + systemInfo.batteryLevel+'%',
            "message": '這裡要打什麼好呢'

        })

    } catch(error) {
        console.log(`fn: batteryLowGrpcAdapter ${error}`)
    }
}

exports.batteryLowGrpcAdapter = batteryLowGrpcAdapter


