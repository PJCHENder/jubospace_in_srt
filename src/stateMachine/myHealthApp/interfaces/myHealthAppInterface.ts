export interface WsConnectToken {
    "uid": string;
    "deviceId": string;
    "patientId": string;
    "isAlive": boolean;
    "ipAddress": string;
    "keepAliveTimeout": string;
}

export interface iPadControllerInterface {
    initController: () => void
    "systemInfo": genericSystemInfo;
    handleSystemInfo: (iPad: iPadDeviceDbInterface, msg:string) => void
    handleIncomingMessage: (iPad: iPadDeviceDbInterface, msg:string) => Promise <any>
    handlePanicButtonRequest: (iPad: iPadDeviceDbInterface,cmd:string) => void
    handleLightControlRequest: (iPad: iPadDeviceDbInterface, cmd:string) => void

}

export type genericSystemInfo = {
    "batteryLevel": string;
}

export interface panicButtonStateInterface {

}



export interface iPadDeviceDbInterface {
    "boxDeviceId": string;
    "deviceId": string;
    "location": DeviceLocation;
    "createdTime": string;
    "updatedTime": string;
    "lastBeatTime": string;
}

type DeviceLocation = {
    "facility": string;
    "roomNum": string;
    "location": string;
}


