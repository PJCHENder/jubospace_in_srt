'use strict'

const mqtt = require('mqtt')
let clientId = 'juboMqtt_' + Math.random().toString(16).substr(2, 8)
const mwgGrpcAdapter = require('./mwgGrpcAdapter')
const mwgApi = require('./mwgGrpcAdapterApi')
const utils = require('./../../utils/utilities')

import { MwgSetting } from './mwgInterface'
import { GenericSpaceInfo } from './../../interfaces/genericInterface'

const mqttConnectOption = {
    "clientId":clientId,
    "username": '',
    "password": '',
    "clean": true,
    "keepAlive": 10,
    "connectTimeout": 2000
}

const pingMessage = {
    "message": "keepAlive"
}

const mwgSubTopicList = ["MwgStatusApi", "MwgSleepInfoApi", ]

const defaultSetting : MwgSetting = {
    mattressMac: "01BA4C1A",
    offBedSensitivity:{
        dayTime:3,
        nightTime:3
    },
    awayFromBedAlarmDuration:{
        dayTime:0,
        nightTime:30
    },
    onBedLongStayDuration:{
        dayTime:120,
        nightTime:0
    },
    dayAndNightSetting:{
        dayTimeStartAt:"07:00:00",
        nightTimeStartAt:"20:00:00"
    }
}

class MwgMqttServer {
    spaceInfo: GenericSpaceInfo
    mqttClientHandler : any
    timerHandler: any

    constructor( spaceInfo : GenericSpaceInfo){
        this.spaceInfo = spaceInfo
        //console.log(this.spaceInfo.tenants)
        this.setMwgMattressSetting(defaultSetting).then((resp)=> {
            mwgGrpcAdapter.bedSettingGrpcAdapter(this.spaceInfo,defaultSetting,'mwg')
        })
        this.mqttClientHandler = mqtt.connect('mqtt://localhost:1884',mqttConnectOption) 
        this.clientMainOp()
    }

    private clientMainOp = () => {
        try{
            this.mqttClientHandler.on('connect',(packet) => {
                //console.log(this.spaceInfo)
                console.log(`Client ${clientId} -> Mosquitto Server: ${this.mqttClientHandler.connected}`)
                if(this.mqttClientHandler.connected === false){
                    this.mqttClientHandler.end()
                    console.log('ouch connected = false')
                } else {
                    if(packet.cmd ==='connack'){
                        this.mqttClientHandler.subscribe(mwgSubTopicList, { qos: 0 })
                    }
                }                
            })

            this.mqttClientHandler.on('reconnect', () => {
                console.log(`Client ${clientId} re-connected`)
            })
            
            this.mqttClientHandler.on('disconnect', (packet) => {
                console.log(`Client ${clientId} disconnected`)
            })
            
            this.mqttClientHandler.on('offline', () => {
                console.log(`Client ${clientId} offline`)
            })
            
            this.mqttClientHandler.on('error',(error) => {
              console.log(`Client ${clientId} error`)
            })
            
            this.mqttClientHandler.on('message', (topic, message, packet) => {
                try{
                    console.log('Received Message:= ' + message.toString() + '\nOn topic:= ' + topic)
                    let updatePacket = JSON.parse(utils.removeByteOrderMark(packet.payload.toString()))
                    let spaceInfo = this.spaceInfo
                    switch(topic){
                        case "MwgStatusApi":
                            updatePacket['timestamp'] = new Date().getTime();
                            console.log(updatePacket)
                            let bedState = mwgApi.mwgAdapterLogic
                            .find((x: { bedState: number; }) => x.bedState === updatePacket.bedState)
                            //console.log(bedState)
                            switch(bedState.stateType){
                                case 'Bed Activity':
                                    mwgGrpcAdapter.bedActivityGrpcAdapter(spaceInfo,updatePacket,{'bedType':'mwg','actionLogic':{}})
                                break;

                                case 'Bed System':
                                    mwgGrpcAdapter.bedSystemGrpcAdapter(spaceInfo,updatePacket,{'bedType':'mwg','actionLogic':{}})
                                break;

                                case 'Bed Alert':
                                    mwgGrpcAdapter.bedAlertGrpcAdapter(spaceInfo,updatePacket,{'bedType':'mwg','actionLogic':{}})
                                break;
                            }
                            //grpcClient.BedStatusUpdate(room)
                        break;
                
                        case "MwgSleepInfoApi":
                            mwgGrpcAdapter.sleepInfoGrpcAdapter(spaceInfo,updatePacket,{'bedType':'mwg','actionLogic':{}})                            
                        break;
                
                        case "MwgSettingApi":
                            
                        break;
                
                        default:
                            console.log('unknown Mqtt Packet')
                            console.log(JSON.parse(updatePacket))
                        break;
                    }  

                } catch (error) {
                    if(error.name === 'TypeError'){
                        console.log(error)
                        throw new Error('MQTT Packet JSON Format Error')
                    }
                }
      
            })            

            this.mqttClientHandler.on('packetreceive',(packet)=> {
                //console.log('packet receive')
                //console.log(packet)
            })
              
            this.mqttClientHandler.on('close', function () {
                this.mqttClientHandler.end()
                console.log(clientId + ' closed')
                clientReConnect()
            
            })
            
            const clientReConnect = () => {
                console.log('re-connect')
                clientId = 'juboMqtt_' + Math.random().toString(16).substr(2, 8)
                this.mqttClientHandler = mqtt.connect('mqtt://0.0.0.0:1883',mqttConnectOption)
            }
                        
        } catch (error) {
            console.log(`fn:clientMainOp ${error}`)
        } finally {
            console.log('mqtt client initializing')
            this.timerHandler = setInterval(this.clientMqttHeartBeat.bind(this),10000)
        }
    }

    private clientMqttHeartBeat = () => {
        try{
            //console.log('mqtt heartbeat')
            this.mqttClientHandler.publish('ping',JSON.stringify(pingMessage))
        } catch (error) {
            console.log(`fn:clientMqttHeartBeat ${error}`)
        }
    }

    public setMwgMattressSetting = async ( mwgSetting : MwgSetting ) => {
        try{
            if(this.mqttClientHandler.connected === true){
               return await this.mqttClientHandler.publish('MwgSettingApi', 
                                                    JSON.stringify(mwgSetting), 
                                                    {
                                                        retain: true,
                                                        qos: 1
                                                    })
            }
        } catch (error) {
            console.log(`fn:setMwgMattressSetting ${error}`)
        }
    }

    public getMwgMattressSetting = async () => {
        try{

        } catch (error) {

        }
    }
}

// test code


/*
let test = new MwgMqttServer()

setInterval(() => {
    test.setMwgMattressSetting(setting).then((resp)=> {
        //console.log(resp)
    })
},30000)
*/
module.exports = MwgMqttServer


/*
terminal test script

mosquitto test sub
mosquitto_sub -p 1884 -t "#"

MwgStatusApi :  
{'mattressMac' : 01BA4C1A,'bedStateAlertLevel' : 0,'bedState' : 15}

mosquitto test Script:
mosquitto_pub -t 'MwgStatusApi' -m "{'mattressMac':'01BA4C1A','bedStateAlertLevel':0,'bedState':15}"

MwgSleepInfoApi:  
{"mattressMac" : "01BA4C1A","sleepStartTime" : "2020-07-20 11:01:40","sleepEndTime" : "2020-07-20 12:03:16","sleepLatency" : 1694,"sleepEffectiveness" : 54.166666666666664,"turnOverCnt" : 5,"notInBedCnt" : 0,"totalNotInBedTime" : 0}

mosquitto test Script:
mosquitto_pub -t 'MwgSleepInfoApi' -m "{'mattressMac':'01BA4C1A','sleepStartTime':'2020-07-20 11:01:40','sleepEndTime':'2020-07-20 12:03:16','sleepLatency':1694,'sleepEffectiveness':54.167,'turnOverCnt':5,'notInBedCnt':0,'totalNotInBedTime':0}"
*/

