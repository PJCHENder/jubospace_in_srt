'use strict'

const gRpc = require('./../../grpc/smileInnGrpcClient').grpcOutwardHandler
const grpcHandler = require('./../../grpc/juboSpaceDeviceClient').grpcDeviceClientHandler
import { MwgSetting } from './mwgInterface'
const mwgApi = require('./mwgGrpcAdapterApi')
const utils = require('./../../utils/utilities')


import { GenericSpaceInfo } from './../../interfaces/genericInterface'

const bedActivityGrpcAdapter = async (spaceInfo:GenericSpaceInfo, bedPacket : any, logic : any  ) => {
    try {
        if(logic.bedType === undefined){throw new Error("Undefined Bed Source calling bedActivityGrpcAdapter")}
        else if(logic.bedType === 'mwg'){
            //console.log(spaceInfo)
            let bedState = mwgApi.mwgAdapterLogic
                            .find((x: { bedState: number; }) => x.bedState === bedPacket.bedState)
            //console.log(bedState)

            await grpcHandler.DeviceEventGrpcUpdate({
                "transactionNo": 0,
                "NISPatientId": spaceInfo.tenant.id_number,
                "boxDeviceId": spaceInfo.deviceInfo.mwgBedMattress.boxDeviceId,
                "name": "MWG Bed Mattress",
                "location": spaceInfo.deviceInfo.mwgBedMattress.location,
                "room": spaceInfo.roomNum,
                "event": bedState.stateType,
                "message": bedState.stateMessage,
                "occurredAt": utils.formatDate(bedPacket.timestamp,"Y-m-d H:i:s")
            })

        }


    } catch(error){
        console.log(`bedActivityGrpcAdapter ${error}`)
    }
}

const bedSystemGrpcAdapter = async (spaceInfo:GenericSpaceInfo, bedPacket : any, logic : any, ) => {
    try{
        //console.log(bedPacket)
        if(logic.bedType === undefined){throw new Error("Undefined Bed Source calling bedSystemGrpcAdapter")}
        else if(logic.bedType === 'mwg'){
            //console.log(spaceInfo)
            let bedState = mwgApi.mwgAdapterLogic
                            .find((x: { bedState: number; }) => x.bedState === bedPacket.bedState)
            //console.log(bedState) 

            await grpcHandler.DeviceEventGrpcUpdate({
                "transactionNo": 0,
                "NISPatientId": spaceInfo.tenant.id_number,
                "boxDeviceId": spaceInfo.deviceInfo.mwgBedMattress.boxDeviceId,
                "name": "MWG Bed Mattress",
                "location": spaceInfo.deviceInfo.mwgBedMattress.location,
                "room": spaceInfo.roomNum,
                "event": bedState.stateType,
                "message": bedState.stateMessage,
                "occurredAt": utils.formatDate(bedPacket.timestamp,"Y-m-d H:i:s")
            })            

        }

    } catch (error) {
        console.log(`bedSystemGrpcAdapter ${error}`)
    }
}

const bedAlertGrpcAdapter = async (spaceInfo:GenericSpaceInfo, bedPacket :any , logic : any) => {
    try{
        //console.log(bedPacket)
        if(logic.bedType === undefined) { throw new Error("Undefined Bed Source calling bedAlertGrpcAdapter")}
        else if (logic.bedType === 'mwg'){
            //console.log(spaceInfo)
            let bedState = mwgApi.mwgAdapterLogic
                .find((x: { bedState: number; }) => x.bedState === bedPacket.bedState)
            //console.log(bedState)

            await grpcHandler.DeviceEventGrpcUpdate({
                "transactionNo": 0,
                "NISPatientId": spaceInfo.tenant.id_number,
                "boxDeviceId": spaceInfo.deviceInfo.mwgBedMattress.boxDeviceId,
                "name": "MWG Bed Mattress",
                "location": spaceInfo.deviceInfo.mwgBedMattress.location,
                "room": spaceInfo.roomNum,
                "event": bedState.stateType,
                "message": bedState.stateMessage,
                "occurredAt": utils.formatDate(bedPacket.timestamp,"Y-m-d H:i:s")
            })
          
        }

    } catch(error){
        console.log(`bedAlertGrpcAdapter ${error}`)
    }
}

const sleepInfoGrpcAdapter = async (spaceInfo:GenericSpaceInfo, bedPacket : any, logic : any) => {
    try {
        //console.log(bedPacket)
        let tenant = spaceInfo.tenants.find((x:{ mattressMac:any }) =>x.mattressMac === bedPacket.mattressMac)
        //console.log(tenant)
        if(logic.bedType === undefined) {throw new Error ("Undefined Bed Source calling sleepInfoGrpcAdapter")}
        else if(logic.bedType === 'mwg'){
            //console.log(spaceInfo)
            await gRpc.SleepInfoReport(        {
                "room": spaceInfo.roomNum.toString(),
                "type": "mwg-sleep-info",
                "mattressMac": bedPacket.mattressMac,
                "sleepStartTime": bedPacket.sleepStartTime,
                "sleepEndTime": bedPacket.sleepEndTime,
                "sleepLatency": bedPacket.sleepLatency.toString(),
                "sleepEffectiveness": bedPacket.sleepEffectiveness.toString(),
                "turnOverCnt": bedPacket.turnOverCnt.toString(),
                "notInBedCnt": bedPacket.notInBedCnt.toString(),
                "totalNotInBedTime": bedPacket.totalNotInBedTime.toString(),
                "bedPacket": '睡眠資訊',
                "patientName": tenant.last_name + tenant.first_name,
                "patientId": tenant.id_number
    
            })
        }
    } catch( error){
        console.log(`sleepInfoGrpcAdapter ${error}`)
    }
}

const bedSettingGrpcAdapter = async (spaceInfo: GenericSpaceInfo, bedPacket: any, source: string) => {
    try{
        console.log(bedPacket)
        await gRpc.BedSettingUpdate(bedPacket)

    } catch(error){
        console.log(`bedSettingGrpcAdapter ${error}`)
    }
}

exports.bedActivityGrpcAdapter = bedActivityGrpcAdapter
exports.bedSystemGrpcAdapter = bedSystemGrpcAdapter
exports.bedAlertGrpcAdapter = bedAlertGrpcAdapter

exports.sleepInfoGrpcAdapter = sleepInfoGrpcAdapter
exports.bedSettingGrpcAdapter = bedSettingGrpcAdapter