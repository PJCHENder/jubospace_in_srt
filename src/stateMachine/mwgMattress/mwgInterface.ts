'use strict'

export interface MwgSetting {
    "mattressMac": string;
    "offBedSensitivity": offBedSensitivity;
    "awayFromBedAlarmDuration": awayFromBedAlarmType;
    "onBedLongStayDuration": onBedLongStayType;
    "dayAndNightSetting": dayAndNightType; 
}

type offBedSensitivity = {
    "dayTime": number;
    "nightTime": number;
}

type awayFromBedAlarmType = {
    "dayTime": number;
    "nightTime": number;
}

type onBedLongStayType = {
    "dayTime": number;
    "nightTime": number;
}

type dayAndNightType = {
    "dayTimeStartAt": string;
    "nightTimeStartAt": string;
}