"use strict";
export {};
const modbus = require("icp-modbus");
const net = require('net')
const JuboEvent = require('./../commonEvent/commonEvent').commonEmitter

const socket = new net.Socket()
const HOST = 'localhost'
const PORT = '502' 
const options = {
    'host': '192.168.2.196',
    'port': '502'
}

interface Modbus {
    socket: any;    //net.Socket()
    connectOption: {
        host: string;
        port: string;
    }
    address: string;
    port: string;
    client: any;
    init();
}

class Modbus {
    constructor(address,port){
        this.address = address || HOST
        this.port = port || PORT
        this.socket = new net.Socket().connect({
            host: this.address,
            port: this.port
        })
        this.client = new modbus.client.TCP(this.socket)  
        this.socket.on('connect', () => {

        }) 
        this.socket.on('close', () => {
            console.log('Client closed');
        });

        this.socket.on('error',(error) => {
            console.log(`Modbus Error: ${error}`);
        })
    }
    public sendInfrared = (irCode) => {
       console.log(this.client)
        return new Promise ((resolve,reject) => {
            this.client.writeMultipleRegisters(1103,irCode)
            .then((resp)=>{
                console.log('ir send')
                resolve(resp)
            }).catch((error)=>{
                reject(error)

            })
        })
    }
}

module.exports = Modbus
