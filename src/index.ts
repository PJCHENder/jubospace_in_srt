'use strict';
export {};

//const sleep = require('util').promisify(setTimeout)
const sysProcess = require('process');

const Netlist = require('./netlist/netlist');
const vayyarController = require('./stateMachine/vayyarWalabot/vayyarWalabotGateway')
const philioGateway = require('./stateMachine/philioZwave/philioGateway')
const myHealthApp = require('./stateMachine/myHealthApp/myHealthAppServer')
const mwgMattressController = require('./stateMachine/mwgMattress/mwgGateway')
const JuboDeviceDbClass = require('./nanoCouchDb/juboDeviceDbClass')
const JuboDeviceDb = new JuboDeviceDbClass()

import type { GenericSpaceInfo, GenericTenantInfo } from './interfaces/genericInterface'

type JuboBox = {
    spaceInfo: GenericSpaceInfo,
    vayyar: any;
    mwg: any;
    philio: any; 
    couchDb: any;   
}

let defaultTenant : GenericTenantInfo =
    {
        "_id": 111,
        "first_name": "咪咪",
        "last_name": "陳",
        "display_name": "Meow",
        "phone": "123-4567",
        "birthday": "1911-11-99",
        "sex": "雙生",
        "email": "123@gmail.com",
        "id_number": "5ed5f57439baab1d94cdb495",
        "roomNum": 100,
        "bedNum": 1,
        "mattressMac": '01BA4C1A'
    }

const smilinnInit = async () => {
    let self= await Netlist.findSelf();
    let couchDbDevices = JuboDeviceDb.devicesInfo

    if(self){
        let mySpace : GenericSpaceInfo = {
            address: '新店區民權路132號2樓',
            roomNum: 100,
            numberOfBeds: 2,
            netInfo: self,
            tenant: defaultTenant,
            deviceInfo : couchDbDevices

        }
        let JuboSpace : any | JuboBox = {
            spaceInfo: mySpace,
            vayyar: new vayyarController(mySpace),
            mwg: new mwgMattressController(mySpace),
            philio: new philioGateway(mySpace,JuboDeviceDb),
            couchDb: JuboDeviceDb
        }
        new myHealthApp(JuboSpace)
    }
}

smilinnInit()

sysProcess.on('uncaughtException', err => {
    console.log(`Uncaught Exception: ${err.message}`)
    sysProcess.exit(1)
})

sysProcess.on("beforeExit", endCode => {
    console.log("Before Exit I execute this after timeout")
    setTimeout(()=>{
        console.log(`Process will exit with code: ${endCode}`)
        process.exit(endCode)
    },100)
})

sysProcess.on('SIGTERM', () => {
    console.log(`Process ${process.pid} received a SIGTERM signal`)
    sysProcess.exit(0)
});

sysProcess.on('SIGINT', signal => {
    console.log(`Process ${sysProcess.pid} has been interrupted`)
    sysProcess.exit(0)
  })

sysProcess.on('exit', function (code) {
    return console.log(`About to exit with code ${code}`);
});
