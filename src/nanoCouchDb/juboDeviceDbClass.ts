'use strict'
const Nano = require('nano')

const user = 'admin'
const pw = '50868012'
const nanoAddress = '127.0.0.1'
const nanoPort = '5984'
let database = 'http://'+ user + ':' + pw + '@' + nanoAddress + ':' + nanoPort
let nano = Nano(database)

interface Tenant {}

const devices = nano.db.use('jubo-space-devices')


class JuboDeviceDb {
    databaseId: string;
    devicesInfo: any;
    timerHandler: any;
    dataReady: boolean

    constructor () {
        this.dataReady = false
        this.getDeviceDbId().then((result)=>{
            this.databaseId = result.rows[0].id
        }).then(()=>{
            this.querySpaceDevices().then((result)=>{
                //console.log(result)
                this.devicesInfo = result
                this.dataReady = true
            })
            
            
        })
        this.timerHandler = setInterval(this.dbTimerRoutine.bind(this),2000)
    }

    private dbTimerRoutine = () => {
        try {
            //this is a test
            //if(test.dataReady){
                //console.log(test.getVayyarDbInfo())
            //}

        } catch (error) {
            console.log(`fn: dbTimerRoutine ${error}`)
        }
    }

    private getDeviceDbId = async () => {
        try{
           let query = await devices.list()
           return query

        } catch (error) {
            console.log(`fn:getDeviceDbId ${error}`)
        }

    }

    private querySpaceDevices = async () => {
         let spaceDevices = await devices.get(this.databaseId)
         return spaceDevices
    }

    public getVayyarDbInfo = () => {
        return this.devicesInfo.vayyarWalabot
    }

    public setVayyarDbInfo = () => {
        console.log('in setVayyarDbInfo')
    }

    public getMwgMattressDbInfo = () => {
        return this.devicesInfo.mwgBedMattress
    }

    public setMwgMattressDbInfo = () => {
        
    }

    public getIpadDbInfo = () => {
        return this.devicesInfo.iPadDevice
    }

    public setIpadDbInfo = () => {

    }

    public getPhilioGatewayDbInfo = () => {
        //console.log(this.devicesInfo)
        return this.devicesInfo.philioGateway
    }

    public setPhilioGatewayDbInfo = () => {

    }
}

module.exports = JuboDeviceDb







