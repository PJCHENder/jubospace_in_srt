

export interface SmileRoomClass {
    roomNumber: number;
    howManyBeds: number;
}