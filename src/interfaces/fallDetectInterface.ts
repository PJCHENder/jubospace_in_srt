
import { GenericFallDetector } from './genericInterface'

export interface VayyarFallDetector extends GenericFallDetector {
    "endTimestamp": number;
}

