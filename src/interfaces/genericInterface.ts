export type GenericSpaceInfo = {
    address: string;
    roomNum: number;
    numberOfBeds: number;
    netInfo: GenericNetworkDevice;
    tenant?: GenericTenantInfo;
    tenants?: Array <GenericTenantInfo>
    deviceInfo: any;

}

export type GenericTenantInfo = {
    "_id": number;
    "first_name": string;
    "last_name": string;
    "display_name": string;
    "phone"?: string;
    "birthday"?: string;
    "sex"?: string;
    "email"?: string;
    "id_number": string;
    "roomNum": number;
    "bedNum": number;
    "mattressMac": string;

}

export interface GenericNetworkDevice {
    "deviceVendor": string;
    "deviceIp": string;
    "deviceMac": string;
    "deviceHostname": string;
    "isDeviceAlive": boolean;
    "connectOption": GenericQueryOption;
    "philioPanId": string;
}

export interface GenericQueryOption {
    uri: string;
    port: number;
    method: string;
    auth: BasicAuthOption;
}

export interface BasicAuthOption {
    user: string;
    pass: string;
    sendImmediately: boolean;
}

export interface GenericFallDetector {
    "deviceId": string;
    "homeId"?: string;
    "detectorType": string;
    "location": string;
    "status": string; 
    "statusUpdateTimestamp": number;   
}

export interface GenericPresenceDetector {
    "deviceId": string;
    "homeId"?: string;
    "detectorType": string;
    "location": string;
    "status": string; 
    "statusUpdateTimestamp": number;   
}

export interface GenericBedSensor {
    "deviceId": string;
    "homeId"?: string;
}

export interface GenericLightController {
    "deviceId": string;
    "homeId"?: string;
}

export interface GenericDimmingController {
    "deviceId": string;
    "homeId"?: string;
    "dimmerType"?: string;
}

export interface GenericSwitchController {
    "deviceId": string;
    "homeId"?: string;
    "switchType"?: string;
}

export interface GenericPanicButton {
    "deviceId": string;
    "homeId"?: string;
    "buttonType"?: string;

}

export interface GenericWIFIDeviceStatus extends GenericDeviceStatus{
    "ssid"?: string | number;
    "signalLevel"?: string | number;
    "linkSpeed"?: string | number;
    "frequency"?: string | number;
    "rssi"?: string | number;
}

export interface GenericBluetoothDeviceStatus extends GenericDeviceStatus{

}

export interface GenericZwaveDeviceStatus extends GenericDeviceStatus{

}

export interface GenericDeviceStatus {

    "batteryLevel"?: string | number;

}