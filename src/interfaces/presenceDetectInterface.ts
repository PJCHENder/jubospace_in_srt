import { GenericPresenceDetector } from './genericInterface'

export interface VayyarPresenceDetector extends GenericPresenceDetector {
    "presentDetected": boolean;
    "roomPresenceIndication": number;
    "presenceTargetType": number;
    "timestamp": number;
    "presenceRegionMap": string;
}

