# Jubo Space

### Server Structure

1.
2.
3.
4.



### Vayyar Docker Setup Procedure

download at: 
https://www.dropbox.com/sh/4mc4q31rhf16lmy/AACtIRE0YkboJGQLUa5xiCdFa?dl=0

go to folder Dashboard Demo/20Nov2019

docker load -i walabotmqttdashboard.tar
docker run -d -p 7777:80 -p 9001:9001 -p 1883:1883 walabotmqttdashboard:dev

### MWG Mattress MQTT Test API

terminal test script
port: 1884

###### Mosquitto test sub all topics
```
mosquitto_sub -p 1884 -t "#"
```
###### MwgStatusApi :  
```
mosquitto_pub -t 'MwgStatusApi' -m "{'mattressMac':'01BA4C1A','bedStateAlertLevel':0,'bedState':15}" -p 1884
```
###### MwgSleepInfoApi:
```
mosquitto_pub -t 'MwgSleepInfoApi' -m "{'mattressMac':'01BA4C1A','sleepStartTime':'2020-07-20 11:01:40','sleepEndTime':'2020-07-20 12:03:16','sleepLatency':1694,'sleepEffectiveness':54.167,'turnOverCnt':5,'notInBedCnt':0,'totalNotInBedTime':0}" -p 1884
```

### CouchDb
docker run -d -p 5984:5984 couchdb:latest



### Task To Do List

- [x] task 1
- [x] task 2
- [x] task 3
- [ ] task 4


